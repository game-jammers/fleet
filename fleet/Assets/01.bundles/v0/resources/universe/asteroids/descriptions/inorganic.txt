A C-Type (Chrondite) which is most common, with a core comprised primarily of clay and silcate rock.
Exposure to high temperatures during formation has caused iron to sink towards the center, and forcing balsaltic (volcanic) lava to the surface.
This astroid has numerous deposits of iron, nickel, and trace amounts of useful titanium which can be used in manufacturing.
The thick deposits of frozen ice on this asteroid are particularly interesting.
