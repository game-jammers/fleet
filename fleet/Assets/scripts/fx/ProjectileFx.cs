//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class ProjectileFx
        : WeaponFx
    {
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(target == null) 
            {
                Destroy(gameObject);
                return;
            }

            Vector3 diff = target.transform.position - transform.position;
            if(diff.sqrMagnitude < 10f)
            {
                Destroy(gameObject);
            }
        }
        
    }
}
