//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class LaserFx
        : WeaponFx
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public RandomizeLine line;

        //
        // unity callbacks /////////////////////////////////////////////////////
        //
    
        protected virtual void LateUpdate()
        {
            if(shooter == null || target == null)
            {
                Destroy(gameObject);
                return;
            }

            line.startpos = shooter.transform.position;
            line.endpos = target.transform.position;
        }
    }
}
