//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class Missile
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vessel shooter { get; private set; }
        public SpaceObject target  { get; private set; }

        public float turnSpeed;
        public float moveSpeed;
        public GameObject explosionFx;

        private Vector3 lastPos;
        private float dist = 1f;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Initialize(Vessel shooter, SpaceObject target)
        {
            if(target == null || shooter == null)
            {
                Explode(false);
            }
            else
            {
                this.shooter = shooter;
                this.target = target;
                lastPos = target.transform.position;
                dist = target.visualSize;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Explode(bool fx)
        {
            Destroy(gameObject);
            if(explosionFx != null && fx)
            {
                Instantiate(explosionFx, transform.position, transform.rotation);
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            Vector3 moveTarget = lastPos;
            if(target != null)
            {
                moveTarget = target.transform.position;
                lastPos = moveTarget;
                dist = target.visualSize;
            }

            Quaternion targetRot = Quaternion.LookRotation(moveTarget - transform.position, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRot, turnSpeed * Time.deltaTime);
            transform.position += transform.forward * moveSpeed * Time.deltaTime;

            if((moveTarget - transform.position).sqrMagnitude <= (dist * dist * 0.5f))
            {
                Explode(true);
            }
        }
    }
}
