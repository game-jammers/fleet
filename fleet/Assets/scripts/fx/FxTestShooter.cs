//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace Fleet
{
    public class FxTestShooter
        : Vessel
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override VesselData data                         { get { return m_data; } protected set { m_data = value; } }
        public VesselData m_data;
        public WeaponFx fx;
        public SpaceObject target;
        public float fireDelay;

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator Fire()
        {
            var wait = new WaitForSeconds(fireDelay);
            while(true)
            {
                Dbg.Log("SHOOT");
                WeaponFx.Create(this, target, fx);
                yield return wait;
            }
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            StartCoroutine(Fire());
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void Update()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void LateUpdate()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void OnDestroy()
        {
        }
        
        
        
    }
}
