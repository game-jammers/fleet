//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class MissileFx
        : WeaponFx
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        public enum Pattern
        {
            Cycle,
            Barrage,
            Blossom
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Pattern pattern;
        public Missile prefab;
        public int numProjectilesPerBurst;
        public int numBursts;
        public float burstGap;
        public float fireGap;

        //
        // coroutines /////////////////////////////////////////////////////////
        //

        private IEnumerator StartFiring()
        {
            for(int i = 0; i < numBursts; ++i)
            {
                yield return FireBurstCycle();
                yield return new WaitForSeconds(burstGap);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator FireBurstCycle()
        {
            float thetaStep = 360f / numProjectilesPerBurst;
            float r = 1f;
            var wait = new WaitForSeconds(fireGap);
            for(int i = 0; i < numProjectilesPerBurst; ++i)
            {
                Vector3 dir = transform.TransformDirection(new Vector3(Mathf.Sin(thetaStep*i), Mathf.Cos(thetaStep*i), 0f).normalized);
                Quaternion rotation = Quaternion.LookRotation(dir, Vector3.up);
                Missile missile = Instantiate(prefab, transform.position + (dir * r), rotation);
                missile.Initialize(shooter, target);
                yield return wait;
            }
        }
        
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            StartCoroutine(StartFiring());
        }
    }
}
