//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class FxTestTarget
        : SpaceObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        public override float visualSize                        { get { return m_size; } }
        public float m_size                                     = 10f;
        public override float jumpWell                          { get { return 0f; } }

        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected override InspectData GetInspectData()         
        { 
            return default(InspectData);
        }
    }
}
