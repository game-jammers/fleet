//
// (c) GameJammers 2019
// http://jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public enum PauseMode
    {
        None,
        Menu,
        Cutscene,
    }

    //
    // ########################################################################
    //
    
    public class SceneManager
        : BaseSceneManager
    {
        //
        // events //////////////////////////////////////////////////////////////
        //

        public delegate void SceneReadyCallback();
        public event SceneReadyCallback OnSceneReady;

        //
        // members /////////////////////////////////////////////////////////////
        //

        public bool forceLock                                   = false;
        public PauseMode pause                                  { get { return m_pause; } set { if(!forceLock) m_pause = value; } }
        private PauseMode m_pause                               = PauseMode.None;

        public static new SceneManager instance                 { get; private set; }
        public GameObjectPools pools                            = new GameObjectPools();
        

        // unity callbacks /////////////////////////////////////////////////////
        protected override void Awake()
        {
            base.Awake();
            instance = this;
            GameManager.EnsureExists();
        }

        // protected methods ///////////////////////////////////////////////////
        protected void NotifySceneReady()
        {
            if( OnSceneReady != null )
            {
                OnSceneReady();
            }
        }
    }
}
