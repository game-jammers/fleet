//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class TitleSceneManager
        : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIElement fader;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public void StartGame()
        {
            StartCoroutine(StartGameAsync(SceneId.SpaceScene));
        }

        //
        // --------------------------------------------------------------------
        //

        public void StartTutorial()
        {
            StartCoroutine(StartGameAsync(SceneId.IntroScene));
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void QuitApplication()
        {
            Application.Quit();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private IEnumerator StartGameAsync(SceneId sceneId)
        {
            GameManager.instance.ResetPlayer();
            yield return fader.ShowAsync(1f);
            GameManager.ChangeScene(sceneId);
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            fader.Show(0f, null);
            fader.Hide(1f, null);
        }
    }
}
