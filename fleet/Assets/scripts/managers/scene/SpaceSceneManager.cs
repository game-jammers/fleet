//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class SpaceSceneManager
        : BaseSpaceSceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
    
        public override SolarSystemData solarSystem             { get { return m_solarSystem; } }
        public SolarSystemData m_solarSystem                    = null;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            fader.Show(0f, null);
            PlayerData player = GameManager.instance.player;
            m_solarSystem = player.universe.nodes[btRandom.Range(1,player.universe.nodes.Length-1)].CreateSolarSystem();
            local.Refresh(solarSystem.stellarObjects.Random(player.rng));
            fader.Hide(1f, null);

            //
            // turn earth into an asteroid belt
            //

            SolarSystemData sol = GameManager.instance.player.universe.solData;
            BeltData earth = BeltData.Create(local.rng, "Earth Debris Field", 3, new Distance(1, DistanceUnit.AstronomicalUnit));
            earth.name = "Earth Debris Field";
            earth.alienInstallation = true;
            sol.stellarObjects[3] = earth;

            //
            // give all self referenced research
            //
            Research[] specialResearch = Database.GetTable<IndustryDb>().GetSpecialResearch();
            foreach(Research res in specialResearch)
            {
                if(player.industry.completeResearch.Contains(res) == false)
                {
                    player.industry.completeResearch.Add(res);
                }
            }
        }
    }
}
