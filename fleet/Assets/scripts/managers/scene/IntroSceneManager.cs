//
// (c) GameJammers 2021
// http://www.jamming.games
//

#if UNITY_EDITOR
//    #define SKIP_CUTSCENE
//    #define QUICK_CUTSCENE
//    #define BATTLE_TEST
//    #define QUICKER_CUTSCENE
#endif

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class IntroSceneManager
        : BaseSpaceSceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Cutscenes")]
        public Inkwell.InkScenePlayer introCutscene;
        public TextAsset[] scenes;

        [Header("References")]
        public VesselData reconData;
        public VesselData fighterData;
        public VesselData cruiserData;

        public VesselData enemyFighter;
        public VesselData enemyCruiser;
        public VesselData enemyBattleship;

        public GameObject explosionFx;

        public Transform enemyBattleshipSpawnPoint;

        public Research fabricationTesting;
        public Research soleSurvivors;

        public override SolarSystemData solarSystem             { get { return m_solarSystem; } }
        private SolarSystemData m_solarSystem;

        public GameObject gameCamObj;
        public GameObject cutsceneCamObj;

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private IEnumerator PlayIntro()
        {
            #if !QUICK_CUTSCENE
                pause = PauseMode.Cutscene;
                local.hud.enableReserveList = false;
                local.hud.menu.manufactureEnabled = false;
                local.hud.menu.researchEnabled = false;
                local.hud.menu.navigationEnabled = false;
                local.hud.Hide(1.0f, null);
                yield return introCutscene.PlayAsync(OnInkEvent);

                pause = PauseMode.None;
                local.hud.enableReserveList = true;
                local.hud.Show(1.0f, null);

                yield return WaitForDeployVessel();

                pause = PauseMode.Cutscene;
                local.hud.Hide(1.0f, null);
                yield return introCutscene.PlayAsync(OnInkEvent);
                local.hud.Show(1.0f, null);
                pause = PauseMode.None;

                yield return WaitForMining();

                GameManager.instance.player.industry.completeResearch.Add(fabricationTesting);

                pause = PauseMode.Cutscene;
                local.hud.Hide(1.0f, null);
                yield return introCutscene.PlayAsync(OnInkEvent);
                local.hud.Show(1.0f, null);
                pause = PauseMode.None;
                local.hud.menu.manufactureEnabled = true;

                yield return WaitForManufacture();

                pause = PauseMode.Cutscene;
                local.hud.Hide(1.0f, null);
                yield return introCutscene.PlayAsync(OnInkEvent);
                local.hud.Show(1.0f, null);
                local.hud.menu.navigationEnabled = true;
                pause = PauseMode.None;
            #endif

            yield return WaitForEarthJump();

        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator OnInkEvent(Inkwell.InkCommand cmd)
        {
            var wait = new WaitForSeconds(0f);
            if(cmd is Inkwell.InkUserEvent ev)
            {
                yield return wait;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator WaitForDeployVessel()
        {
            introCutscene.sceneText = scenes[0];
            introCutscene.Load();

            var wait = new WaitForSeconds(0.5f);
            while(local.vessels.Count == 0)
                yield return wait;

            while(local.vessels[0].state.mode != VesselState.Mode.Deployed)
                yield return wait;
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator WaitForMining()
        {
            introCutscene.sceneText = scenes[1];
            introCutscene.Load();

            var wait = new WaitForSeconds(0.5f);
            while(local.vessels[0].state.storage[ResourceType.Inorganic].volume < 100)
                yield return wait;

            local.vessels[0].ClearCommand();
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator WaitForManufacture()
        {
            introCutscene.sceneText = scenes[2];
            introCutscene.Load();

            var wait = new WaitForSeconds(0.5f);
            while(GameManager.instance.player.industry.manufactureJobs.Count <= 0)
                yield return wait;

            // time is paused while viewing the manufacturing window
            // make sure you close the window to start the building process
            pause = PauseMode.Cutscene;
            local.hud.Hide(1.0f, null);
            yield return introCutscene.PlayAsync(OnInkEvent);
            local.hud.Show(1.0f, null);

            local.hud.menu.manufacture.Show(0f, null);

            introCutscene.sceneText = scenes[3];
            introCutscene.Load();

            FleetData.VesselEntry flagship = GameManager.instance.player.fleet.flagship;
            while(flagship.state.fuel < flagship.data.stats.maxFuel)
                yield return wait;
        }
        
        //
        // --------------------------------------------------------------------
        //

        private Vessel PlaceVessel(VesselData data, Vector3 position, ControllerType ctype = ControllerType.AI, bool jumpIn = true)
        {
            position.y = 0f;
            return PlaceVesselAnywhere(data, position, ctype, jumpIn);
        }

        //
        // --------------------------------------------------------------------
        //

        private Vessel PlaceVesselAnywhere(VesselData data, Vector3 position, ControllerType ctype = ControllerType.AI, bool jumpIn = true)
        {
            VesselState vstate = VesselState.Create(data.name, data);
            Vessel v = Vessel.JumpIn(local.vesselPrefab, data, vstate, new Xform() {
                position = position,
                rotation = Quaternion.identity,
                localScale = Vector3.one
            });

            vstate.ai = AIType.Aggressive;
            vstate.dontAttackRecon = true;
            if(!jumpIn)
            {
                vstate.mode = VesselState.Mode.Deployed;
            }

            v.controller = ctype;

            local.vessels.Add(v);

            return v;
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator WaitForEarthJump()
        {
            #if !QUICKER_CUTSCENE
            introCutscene.sceneText = scenes[4];
            introCutscene.Load();

            var wait = new WaitForSeconds(0.5f);
            while(local.data.name.StartsWith("Earth") == false)
                yield return wait;

            pause = PauseMode.Cutscene;
            local.hud.Hide(1.0f, null);
            yield return introCutscene.PlayAsync(OnInkEvent);
            local.hud.Show(1.0f, null);
            pause = PauseMode.None;

            Vessel fighter = PlaceVessel(fighterData, Vector3.zero, ControllerType.Player, false);
            Vessel fighter2 = PlaceVessel(fighterData, new Vector3(10f, 0f, 0f), ControllerType.Player, false);
            Vessel cruiser = PlaceVessel(cruiserData, new Vector3(20f, 0f, -10f), ControllerType.Player, false);
            
            while(GameManager.instance.player.fleet.flagship.state.mode != VesselState.Mode.TransitEnter)
                yield return wait;

            Vessel flagship = local.vessels.Find(v=>v.data.type == VesselType.Recon);

            introCutscene.sceneText = scenes[5];
            introCutscene.Load();

            yield return new WaitForSeconds(1f);

            // A FIGHT!!
            pause = PauseMode.Cutscene;
            local.hud.Hide(1.0f, null);
            yield return introCutscene.PlayAsync(OnInkEvent);
            local.hud.Show(1.0f, null);
            pause = PauseMode.None;

            for(int i = 0; i < 5; ++i)
            {
                PlaceVessel(enemyFighter, Vector3Extension.RandomUnit() * 25f * (local.rng.NextUnit() + 0.5f));
                yield return new WaitForSeconds(local.rng.NextUnit() * 1f);
            }

            yield return new WaitForSeconds(5f);

            fighter.state.hull = 0;

            yield return new WaitForSeconds(3f);

            fighter2.state.hull = 0;

            yield return new WaitForSeconds(1f);

            introCutscene.sceneText = scenes[6];
            introCutscene.Load();

            // we can't hang on...
            pause = PauseMode.Cutscene;
            local.hud.Hide(1.0f, null);
            yield return introCutscene.PlayAsync(OnInkEvent);
            local.hud.Show(1.0f, null);
            pause = PauseMode.None;

            for(int i = 0; i < 3; ++i)
            {
                PlaceVessel(enemyFighter, Vector3Extension.RandomUnit() * 25f * (local.rng.NextUnit() + 0.5f));
                yield return new WaitForSeconds(0f);
                PlaceVessel(enemyCruiser, new Vector3(1f, 0f, 1f) * i * local.rng.NextUnit() * 30f);
                yield return new WaitForSeconds(0f);
            }

            yield return new WaitForSeconds(3f);

            for( int i = 0; i < 5; ++i )
            {
                PlaceVessel(enemyBattleship, enemyBattleshipSpawnPoint.position + Vector3Extension.RandomUnit() * 20f);
                yield return new WaitForSeconds(0f);
            }

            yield return new WaitForSeconds(2f);

            introCutscene.sceneText = scenes[7];
            introCutscene.Load();

            // RUUUNNNN
            pause = PauseMode.Cutscene;
            local.hud.Hide(1.0f, null);
            yield return introCutscene.PlayAsync(OnInkEvent);
            local.hud.Show(1.0f, null);
            pause = PauseMode.None;

            flagship.JumpOut();
            int c = 1;
            while(flagship.state.mode != VesselState.Mode.Reserve || c < 200)
            {
                Instantiate(explosionFx, local.bgObject.transform.position + (Vector3Extension.RandomUnit() * 250f), Quaternion.identity);

                if(local.rng.NextUnit() > 0.8f)
                {
                    PlaceVesselAnywhere(enemyBattleship, Vector3Extension.RandomUnit() * local.rng.Range(-100f,100f));
                }

                yield return new WaitForSeconds(0.5f/c);
                ++c;
            }

            fader.Show(3f, null);
            for(int i = 0; i < 300; ++i)
            {
                Instantiate(explosionFx, local.bgObject.transform.position + (Vector3Extension.RandomUnit() * 250f), Quaternion.identity);
                yield return new WaitForSeconds(3f/300f);
            }

            #endif

            pause = PauseMode.Cutscene;
            forceLock = true;

            fader.Show(0f, null);

            Destroy(local.root);
            gameCamObj.SetActive(false);
            cutsceneCamObj.SetActive(true);

            introCutscene.sceneText = scenes[8];
            introCutscene.Load();
            Destroy(local.hud.gameObject);
            yield return introCutscene.PlayAsync(OnInkEvent);

            GameManager.ChangeScene(SceneId.SpaceScene);
        }

        #if UNITY_EDITOR
        public IEnumerator BattleTest()
        {
            PlaceVessel(fighterData, new Vector3(-100f, 0f, 0f), ControllerType.Player, false);
            PlaceVessel(enemyFighter, new Vector3(100f, 0f, 0f));
            yield return new WaitForSeconds(0f);
        }
        #endif
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            fader.Show(0f, null);
            m_solarSystem = GameManager.instance.player.universe.nodes[0].CreateSolarSystem();
            local.Refresh(solarSystem.stellarObjects[4]);

            GameManager.instance.player.fleet.flagship.state.fuel -= 10;

            #if SKIP_CUTSCENE
                fader.Hide(0f, null);
                local.hud.Show(1.0f, null);
                pause = PauseMode.None;
                #if BATTLE_TEST
                    StartCoroutine(BattleTest());
                #endif
            #else
                #if QUICK_CUTSCENE
                    fader.Hide(0f, null);
                    local.hud.Show(1.0f, null);
                    pause = PauseMode.None;
                #endif

                StartCoroutine(PlayIntro());
            #endif
        }
    }
}
