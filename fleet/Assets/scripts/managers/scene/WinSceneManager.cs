//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class WinSceneManager
        : SceneManager
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        //
        // members ////////////////////////////////////////////////////////////
        //

        public Inkwell.InkScenePlayer player;
        public UIElement fader;

        [Header("Section 01")]
        public GameObject cameraPosition;
        public UIElement[] pages01;
        public float fadeInTime = 2f;
        public float fadeOutTime = 2f;
        public float holdTime = 2f;

        public UIElement finalPage;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Restart()
        {
            StartCoroutine(RestartAsync());
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator RestartAsync()
        {
            yield return fader.ShowAsync(1f);
            GameManager.ChangeScene(SceneId.TitleScene);
        }
        
        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator StartCutscene()
        {
            yield return fader.HideAsync(10f);

            UIElement previousPage = null;
            foreach(UIElement page in pages01)
            {
                if(previousPage != null)
                {
                    previousPage.Hide(fadeOutTime, null);
                }

                yield return page.ShowAsync(fadeInTime);
                yield return new WaitForSeconds(holdTime);
                previousPage = page;
            }
            yield return previousPage.HideAsync(fadeOutTime);
            yield return new WaitForSeconds(2f);
            yield return finalPage.ShowAsync(5f);
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            fader.Show(0f, null);
            foreach(UIElement page in pages01)
            {
                page.Hide(0f, null);
            }

            StartCoroutine(StartCutscene());
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator OnInkEvent(Inkwell.InkCommand cmd)
        {
            var wait = new WaitForSeconds(0f);
            if(cmd is Inkwell.InkUserEvent ev)
            {
                yield return wait;
            }
        }
    }
}
