//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public abstract class BaseSpaceSceneManager
        : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Local")]
        public LocalController local;
        public abstract SolarSystemData solarSystem             { get; }

        [Header("Interface")]
        public UIElement fader;

        public static new BaseSpaceSceneManager instance        { get { return SceneManager.instance as BaseSpaceSceneManager; } }
    }
}

