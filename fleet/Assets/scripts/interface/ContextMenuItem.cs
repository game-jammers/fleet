//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class ContextMenuItem
        : UIListItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public ContextMenu.Command command                      { get { return (ContextMenu.Command)data; } }

        public TextMeshProUGUI buttonText;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();

            buttonText.text = command.title;

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnClick()
        {
            BaseSpaceSceneManager.instance.local.ctx.Hide(0.5f, null);
            command.callback();
        }
    }
}
