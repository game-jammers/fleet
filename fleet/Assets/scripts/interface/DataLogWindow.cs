//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class DataLogWindow
        : SpaceWindow
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Colors")]
        public Color selectedColor;
        public Color normalColor;
        public Image mechanicImage;
        public Image loreImage;

        public UIList datalist;
        public IndustryData data                                { get { return GameManager.instance.player.industry; } }
        public DataLog.Type type                                = DataLog.Type.Mechanic;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            switch(type)
            {
                case DataLog.Type.Mechanic:
                    datalist.SetData(data.unlockedMechanicLogs);
                    mechanicImage.color = selectedColor;
                    loreImage.color = normalColor;
                break;
                case DataLog.Type.Lore:
                    datalist.SetData(data.unlockedLoreLogs);
                    mechanicImage.color = normalColor;
                    loreImage.color = selectedColor;
                break;
            }

            return true;
        }

        //
        // --------------------------------------------------------------------
        //

        public void ShowMechanics()
        {
            type = DataLog.Type.Mechanic;
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ShowLore()
        {
            type = DataLog.Type.Lore;
            Refresh();
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override void StartHide()
        {
            base.StartHide();
            type = DataLog.Type.Mechanic;
        }
        
    }
}

