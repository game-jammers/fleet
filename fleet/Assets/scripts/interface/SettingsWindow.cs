//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class SettingsWindow
        : SpaceWindow
    {
        public void QuitGame()
        {
            Application.Quit();
        }
    }
} 
