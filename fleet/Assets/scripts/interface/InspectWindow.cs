//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public struct InspectData
    {
        public Sprite icon;
        public string title;
        public string description;
    }

    public class InspectWindow
        : UIWindow
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public InspectData target                               { get; private set; }

        public Image icon;
        public TextMeshProUGUI title;
        public TextMeshProUGUI description;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public virtual void Show(float time, InspectData target)
        {
            this.target = target;
            Show(time, null);
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();
            if(!result) return false;

            icon.sprite = target.icon;
            title.text = target.title;
            description.text = target.description;

            return true;
        }
        
    }
}
