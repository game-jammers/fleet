//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Fleet
{
    public class SiegeWarningWindow
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public IStellarData location;
        public TextMeshProUGUI locationName;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            if(location != null)
            {
                locationName.text = location.name;
            }
            return true;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Jump()
        {
            BaseSpaceSceneManager.instance.local.StartJump(location);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator CheckAsync()
        {
            var wait = new WaitForSeconds(0.1f);
            while(true)
            {
                if(location == null)
                {
                    foreach(FleetData.StaticEntry entry in GameManager.instance.player.fleet.statics)
                    {
                        if(entry.location.besieged)
                        {
                            location = entry.location;
                            break;
                        }
                    }
                }
                else
                {
                    if(location.besieged == false)
                    {
                        location = null;
                    }
                }

                if(location == null) 
                {
                    Hide(0f, null);
                }
                else       
                {
                    Show(0f, null);
                    Refresh();
                }
                yield return wait;
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            StartCoroutine(CheckAsync());
        }
    }
}
