//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class VesselEntryDragItem
        : UIElement
    {
        public Image icon;
    }
}
