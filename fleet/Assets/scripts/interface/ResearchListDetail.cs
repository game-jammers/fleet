//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class ResearchListDetail
        : UIListDetail
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public new ResearchListItem item                        { get { return base.item as ResearchListItem; } }
        public Research research                                { get { return item.research; } }

        public Image image;
        public TextMeshProUGUI title;
        public TextMeshProUGUI description;
        public TextMeshProUGUI cost;
        public UIList unlocksRecipes;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            image.sprite = research.image;
            title.text = research.displayName;
            cost.text = System.String.Format("{0} minutes", (research.difficulty / 60f).ToString("F2"));
            description.text = research.description.text;
            unlocksRecipes.SetData(research.unlocksRecipes);

            return true;
        }
        
        //
        // --------------------------------------------------------------------
        //

        public void BeginResearch()
        {
            GameManager.instance.player.industry.StartResearch(research);
            list.SelectItem(-1);
            list.Refresh();
        }
    }
}

