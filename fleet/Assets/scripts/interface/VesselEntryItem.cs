//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Fleet
{
    public class VesselEntryItem
        : UIListItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public FleetData.VesselEntry vessel                     { get { return GetVesselEntry(); } }

        public Image icon;
        public Image progress;
        public UISimpleMeter shield;
        public UISimpleMeter armor;
        public UISimpleMeter hull;

        public Color selected;
        public Color normal;
        public Image selection;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();
            
            icon.sprite = vessel.data.icon;
            shield.SetValue(vessel.state.shields / (float)vessel.data.stats.maxShield);
            armor.SetValue(vessel.state.armor / (float)vessel.data.stats.maxArmor);
            hull.SetValue(vessel.state.hull / (float)vessel.data.stats.maxHull);

            selection.color = normal;
            Vessel sov = BaseSpaceSceneManager.instance.local.vessels.Find(v=>v.data == vessel.data);
            if(sov != null && sov.selected)
                selection.color = selected;

            switch(vessel.state.mode)
            {
                case VesselState.Mode.Reserve:
                    isDraggable = true;
                    progress.fillAmount = 0f;
                break;

                case VesselState.Mode.TransitEnter:
                case VesselState.Mode.TransitExit:
                    isDraggable = false;
                    progress.fillAmount = btMath.Clamp(vessel.state.transitAmt / vessel.data.stats.jumpSpeed, 0f, 1f);
                break;

                case VesselState.Mode.Deployed:
                    isDraggable = false;
                    progress.fillAmount = 0f;
                break;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public override void Select()
        {
            base.Select();
            LocalController local = BaseSpaceSceneManager.instance.local;
            Vessel v = local.vessels.Find((v)=>v.state == vessel.state);
            local.DeselectAll();
            local.Select(v);
            local.CenterCameraOn(v);
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override UIElement MakeDragDisplay()
        {
            VesselEntryDragItem item = base.MakeDragDisplay() as VesselEntryDragItem;

            if(item != null)
            {
                item.icon.sprite = icon.sprite;
            }

            return item;
        }

        //
        // --------------------------------------------------------------------
        //

        public override void OnBeginDrag(PointerEventData ev)
        {
            base.OnBeginDrag(ev);
            if(isDraggable)
            {
                BaseSpaceSceneManager.instance.local.BeginDragSpawn(vessel); 
            }
        }
        

        //
        // --------------------------------------------------------------------
        //

        public override void OnDrag(PointerEventData ev)
        {
            base.OnDrag(ev);
            if(isDraggable)
            {
                BaseSpaceSceneManager.instance.local.UpdateDragSpawn(vessel);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public override void OnEndDrag(PointerEventData ev)
        {
            if(!isDraggable) return;
            base.OnEndDrag(ev);
            BaseSpaceSceneManager.instance.local.EndDragSpawn(vessel);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private IEnumerator UpdateAsync()
        {
            var wait = new WaitForSeconds(0.1f);
            while(true)
            {
                Refresh();
                yield return wait;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private FleetData.VesselEntry GetVesselEntry()
        {
            if(data is Vessel vessel)
            {
                return new FleetData.VesselEntry() {
                    data = vessel.data,
                    state = vessel.state
                };
            }
            else if(data is FleetData.VesselEntry entry)
            {
                return entry;
            }

            return default(FleetData.VesselEntry);
        }
        

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            StartCoroutine(UpdateAsync());
        }
        
    }
}
