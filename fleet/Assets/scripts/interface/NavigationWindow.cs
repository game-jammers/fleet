//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class NavigationWindow
        : SpaceWindow
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum View
        {
            SolarSystem,
            Universe
        }

        //
        // --------------------------------------------------------------------
        //
        
        public enum Action
        {
            CameraMoveX,
            CameraMoveY
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Color normal;
        public Color highlighted;
        public Image solarView;
        public Image uniView;
        private View view                                       = View.SolarSystem;

        public UIElement content;
        public UIElement selectedDisplay;
        public TextMeshProUGUI selectedName;

        public UIElement rootPrefab;
        public NavigationObject largePrefab;
        public NavigationObject smallPrefab;
        public UIElement circlePrefab;

        public SolarSystemData solarData                        { get { return BaseSpaceSceneManager.instance.solarSystem; } }
        public UniverseData universeData                        { get { return GameManager.instance.player.universe; } }

        private InputRouter<Action> input;
        [ReadOnly, SerializeField] private Vector2 offset                                  = Vector2.zero;
        public Vector2 scrollSpeed                              = Vector2.one;

        private UIElement root;
        private IStellarData selectedStellarObject              = null;
        private UniverseData.Node selectedUniverseNode          = null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            if(root != null)
            {
                Destroy(root.gameObject);
            }

            root = Instantiate(rootPrefab, Vector3.zero, Quaternion.identity);
            root.transform.SetParent(content.transform);
            root.rectTransform.offsetMin = Vector2.zero;
            root.rectTransform.offsetMax = Vector2.zero;

            switch(view)
            {
                case View.SolarSystem: 
                    solarView.color = highlighted;
                    uniView.color = normal;
                    RefreshSolarSystem(); 
                break;
                case View.Universe: 
                    solarView.color = normal;
                    uniView.color = highlighted;
                    RefreshUniverse(); 
                break;
            }

            return true;
        }

        //
        // --------------------------------------------------------------------
        //

        public void ShowSolarSystem()
        {
            view = View.SolarSystem;
            selectedStellarObject = null;
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ShowUniverse()
        {
            view = View.Universe;
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ResetView()
        {
            offset = Vector3.zero;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Select(IStellarData data)
        {
            selectedStellarObject = data;
            RefreshStellarLocation();
        }

        //
        // --------------------------------------------------------------------
        //

        public void Select(UniverseData.Node data)
        {
            selectedUniverseNode = data;
            RefreshUniverseLocation();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Jump()
        {
            switch(view)
            {
                case View.SolarSystem:
                    BaseSpaceSceneManager.instance.local.StartJump(selectedStellarObject);
                break;
                case View.Universe:
                    BaseSpaceSceneManager.instance.local.StartJump(selectedUniverseNode);
                break;
            }
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override void StartHide()
        {
            base.StartHide();
            view = View.SolarSystem;
        }
        
        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void RefreshSolarSystem()
        {
            System.Random rng = new System.Random(solarData.seed);

            int i = 0;
            foreach(IStellarData sobj in solarData.stellarObjects)
            {
                float theta = rng.Next(-180f, 180f);
                float r = i++ * 80f;
                Vector2 planetPos = new Vector2(r * btMath.Sin(theta), r * btMath.Cos(theta) ) + offset;

                UIElement circle = Instantiate(circlePrefab, Vector3.zero, Quaternion.identity);
                circle.transform.SetParent(root.transform);
                circle.rectTransform.anchoredPosition = Vector2.zero;
                circle.rectTransform.sizeDelta = new Vector2(planetPos.magnitude, 2f);
                circle.rectTransform.localRotation = Quaternion.Euler(0f, 0f, Vector2.SignedAngle(Vector2.right, planetPos.normalized));
                
                NavigationObject nav = Instantiate(largePrefab, Vector3.zero, Quaternion.identity);
                nav.transform.SetParent(root.transform);
                nav.Refresh(sobj);
                nav.rectTransform.anchoredPosition = planetPos;

                if(sobj is PlanetData planet)
                {
                    if(planet.moons != null && planet.moons.Length > 0)
                    {
                        const float STEP = 50f;
                        float startx = planet.moons.Length * -STEP / 2f;
                        int j = 0;
                        foreach(PlanetData moon in planet.moons)
                        {
                            NavigationObject mnav = Instantiate(smallPrefab, Vector3.zero, Quaternion.identity);
                            mnav.transform.SetParent(root.transform);
                            mnav.Refresh(moon);
                            Vector2 moonPos = new Vector2(startx + j * STEP, -STEP);
                            mnav.rectTransform.anchoredPosition = planetPos + moonPos;
                            ++j;
                        }
                    }
                }
            }                
            

            RefreshStellarLocation();
        }

        //
        // --------------------------------------------------------------------
        //

        private void RefreshUniverse()
        {
            System.Random rng = new System.Random(universeData.seed);

            foreach(UniverseData.Node node in universeData.nodes)
            {
                NavigationObject nav = Instantiate(largePrefab, Vector3.zero, Quaternion.identity);
                nav.transform.SetParent(root.transform);
                nav.Refresh(node);
                nav.rectTransform.anchoredPosition = node.position;
            }

            RefreshUniverseLocation();

        }

        //
        // --------------------------------------------------------------------
        //

        private void RefreshStellarLocation()
        {
            if(selectedStellarObject == null)
            {
                selectedDisplay.Hide(0f, null);
            }
            else
            {
                selectedDisplay.Show(0f, null);
                selectedName.text = selectedStellarObject.name;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void RefreshUniverseLocation()
        {
            if(selectedUniverseNode == null)
            {
                selectedDisplay.Hide(0f, null);
            }
            else
            {
                selectedDisplay.Show(0f, null);
                selectedName.text = selectedUniverseNode.id;
            }
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            input = new InputRouter<Action>();
            input.Bind(Action.CameraMoveX, InputAction.FromAxis(KeyCode.A, KeyCode.D));
            input.Bind(Action.CameraMoveY, InputAction.FromAxis(KeyCode.W, KeyCode.S));
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Update()
        {
            if(!visible) return;

            offset += new Vector2(
                input.GetAxis(Action.CameraMoveX) * -scrollSpeed.x,
                input.GetAxis(Action.CameraMoveY) * scrollSpeed.y
            ) * Time.deltaTime;

            content.rectTransform.anchoredPosition = offset;
        }
        
    }
}
