//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class ContextMenu
        : UIElement
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public struct Command
        {
            public string title;
            public System.Action callback;
        };
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vessel owner                                     { get { return sender as Vessel; } }
        public SpaceObject sender                               { get; private set; }
        public SpaceObject target                               { get; private set; }
        public UIList buttons;

        public List<Command> commands                           = new List<Command>();

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public virtual void Show(SpaceObject sender, SpaceObject target)
        {
            Dbg.Assert(target != null, "Attempting to show context menu for a null target");

            this.sender = sender;
            this.target = target;
            Show(0.5f, null);
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();
            
            commands.Clear();

            if(target is Vessel vtarget)
            {
                commands.Add( new Command() {
                    title = "Warp Out",
                    callback = ()=>{
                        vtarget.JumpOut();
                    }
                });
            }
            else
            {
                commands.Add( new Command() {
                    title = "Inspect",
                    callback = ()=>{
                        BaseSpaceSceneManager.instance.local.Inspect(target);
                    }
                });
            }

            if(owner != null && owner.controller == ControllerType.Player)
            {
                foreach(VesselCap cap in owner.data.caps)
                {
                    switch(cap)
                    {
                        case VesselCap.Attack: TryAddAttack(); break;
                        case VesselCap.MiningOrganic: TryAddMiningOrganic(); break;
                        case VesselCap.MiningInorganic: TryAddMiningInorganic(); break;
                        case VesselCap.MiningExotic: TryAddMiningExotic(); break;
                    }
                }
            }

            buttons.SetData(commands);
            return result;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void AddCommand(string title, System.Action cb)
        {
            commands.Add(new Command() {
                title = title,
                callback = cb
            });
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void TryAddAttack()
        {
            if(target is Vessel v && v.controller != ControllerType.Player)
            {
                AddCommand("Attack", ()=>{
                    owner.Attack(v);
                });
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void TryAddMiningOrganic()
        {
            if(target is Asteroid a && a.data.organic > 0)
            {
                string name = System.String.Format( "Mine Organic ({0})", a.data.organic);
                AddCommand(name, ()=>{
                    owner.Mine(ResourceType.Organic, a);
                });
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void TryAddMiningInorganic()
        {
            if(target is Asteroid a && a.data.inorganic > 0)
            {
                string name = System.String.Format( "Mine Inorganic ({0})", a.data.inorganic);
                AddCommand(name, ()=>{
                    owner.Mine(ResourceType.Inorganic, a);
                });
            }
        }

        
        //
        // --------------------------------------------------------------------
        //

        private void TryAddMiningExotic()
        {
            if(target is Asteroid a && a.data.exotic > 0)
            {
                string name = System.String.Format( "Mine Exotic ({0})", a.data.exotic);
                AddCommand(name, ()=>{
                    owner.Mine(ResourceType.Exotic, a);
                });
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            Hide(0f, null);
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Update()
        {
            if(target != null)
            {
                buttons.MoveToWorldPos(SceneManager.instance.sceneCam, target.transform.position);
            }
        }
    }
}
