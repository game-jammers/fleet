//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class ManufactureWindow
        : SpaceWindow
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIList recipeList;
        public UIList manufactureJob;
        public IndustryData data                                { get { return GameManager.instance.player.industry; } }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            recipeList.SetData(data.unlockedRecipes);
            manufactureJob.SetData(data.manufactureJobs);

            return true;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator RefreshAsync()
        {
            var wait = new WaitForSeconds(0.5f);
            while(true)
            {
                Refresh();
                yield return wait;
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            StartCoroutine(RefreshAsync());
        }
        
    }
}
