//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class RecipeListItem
        : UIListItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Recipe recipe                                    { get { return data as Recipe; } }

        public TextMeshProUGUI buttonText;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();

            buttonText.text = recipe.displayName;

            return result;
        }
    }
}

