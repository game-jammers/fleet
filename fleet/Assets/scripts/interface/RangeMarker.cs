//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class RangeMarker
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Renderer render;
        public float range                                      { get { return m_range; } set { m_range = value; Refresh(); } }
        public bool valid                                       { get { return m_valid; } set { m_valid = value; Refresh(); } }

        private float m_range                                   = 1f;
        private bool m_valid                                    = true;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh()
        {
            render.transform.localScale = Vector3.one * (range * 2f);
            render.material.SetFloat("_Valid", valid ? 1f : 0f);
        }
    }
}
