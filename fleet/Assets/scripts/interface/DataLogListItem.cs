//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class DataLogListItem
        : UIListItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public IndustryData industry                            { get { return GameManager.instance.player.industry; } }
        public DataLog log                                      { get { return data as DataLog; } }

        public TextMeshProUGUI buttonText;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            buttonText.text = log.displayName;

            return true;
        }
    }
}

