//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class SpaceWindow
        : UIWindow
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override void StartShow()
        {
            SceneManager.instance.pause = PauseMode.Menu;
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected override void StartHide()
        {
            SceneManager.instance.pause = PauseMode.None;
        }
    }
}
