//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class ResearchJobListItem
        : UIListItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public ResearchJob job                               { get { return data as ResearchJob; } }

        public TextMeshProUGUI buttonText;
        public UISimpleMeter progressMeter;                  

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();

            buttonText.text = job.research.displayName;
            progressMeter.progress = job.perc;

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public void CancelJob()
        {
            GameManager.instance.player.industry.CancelJob(job);
        }
    }
}

