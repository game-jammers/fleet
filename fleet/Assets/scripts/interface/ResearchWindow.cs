//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class ResearchWindow
        : SpaceWindow
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum ResearchType
        {
            Complete,
            Available
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Colors")]
        public Color selectedColor;
        public Color normalColor;

        public ResearchType researchType                        = ResearchType.Available;
        public TextMeshProUGUI researchTypeTitle;

        public Image completeImage;
        public Image availableImage;
        public UIList researchList;
        public UIList activeResearch;

        public IndustryData data                                { get { return GameManager.instance.player.industry; } }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            switch(researchType)
            {
                case ResearchType.Complete: 
                    researchList.SetData(data.completeResearch); 
                    completeImage.color = selectedColor;
                    availableImage.color = normalColor;
                break;
                case ResearchType.Available: 
                    availableImage.color = selectedColor;
                    completeImage.color = normalColor;
                    researchList.SetData(data.availableResearch); 
                break;
            }

            activeResearch.SetData(data.researchJobs);

            return true;
        }

        //
        // --------------------------------------------------------------------
        //

        public void ShowComplete()
        {
            researchType = ResearchType.Complete;
            researchList.SelectItem(-1);
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ShowAvailable()
        {
            researchType = ResearchType.Available;
            researchList.SelectItem(-1);
            Refresh();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator RefreshAsync()
        {
            var wait = new WaitForSeconds(0.5f);
            while(true)
            {
                Refresh();
                yield return wait;
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            StartCoroutine(RefreshAsync());
        }
        
    }
}
