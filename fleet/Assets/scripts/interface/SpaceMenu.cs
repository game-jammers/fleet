//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class SpaceMenu
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIElement manufactureButton;
        public UIElement researchButton;
        public UIElement navigationButton;
        public UIElement settingsButton;

        public BaseSpaceSceneManager sm                         { get { return BaseSpaceSceneManager.instance; } }
        public LocalController local                            { get { return sm.local; } }
        
        public ResearchWindow research;
        public ManufactureWindow manufacture;
        public NavigationWindow navigation;
        public DataLogWindow datalog;
        public SettingsWindow settings;
        
        public bool manufactureEnabled                          { get { return m_manufactureEnabled; } set { m_manufactureEnabled = value; Refresh(); } }
        public bool researchEnabled                             { get { return m_researchEnabled; } set { m_researchEnabled = value; Refresh(); } }
        public bool navigationEnabled                           { get { return m_navigationEnabled; } set { m_navigationEnabled = value; Refresh(); } }

        private bool m_manufactureEnabled                       = true;
        private bool m_researchEnabled                          = true;
        private bool m_navigationEnabled                        = true;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Initialize()
        {
            base.Initialize();
            Close();
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Refresh()
        {
            base.Refresh();
            manufactureButton.Fade(manufactureEnabled, 0.5f, null);
            researchButton.Fade(researchEnabled, 0.5f, null);
            navigationButton.Fade(navigationEnabled, 0.5f, null);
            return true;
        }

        //
        // --------------------------------------------------------------------
        //

        public void OpenResearch()
        {
            if(research.visible) return;
            Close();
            research.Show(0f, null);
        }

        //
        // --------------------------------------------------------------------
        //

        public void OpenManufacture()
        {
            if(manufacture.visible) return;
            Close();
            manufacture.Show(0f, null);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void OpenNavigation()
        {
            if(navigation.visible) return;
            Close();
            navigation.Show(0f, null);
        }

        //
        // --------------------------------------------------------------------
        //

        public void OpenDatalog()
        {
            if(datalog.visible) return;
            Close();
            datalog.Show(0f, null);
        }        

        //
        // --------------------------------------------------------------------
        //

        public void OpenSettings()
        {
            if(settings.visible) return;
            Close();
            settings.Show(0f, null);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Close()
        {
            if(research.visible) research.Hide(0f, null);
            if(manufacture.visible) manufacture.Hide(0f, null);
            if(navigation.visible) navigation.Hide(0f, null);
            if(datalog.visible) datalog.Hide(0f, null);
            if(settings.visible) settings.Hide(0f, null);
        }
    }
}
