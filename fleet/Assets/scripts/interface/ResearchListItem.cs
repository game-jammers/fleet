//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class ResearchListItem
        : UIListItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Research research                                { get { return data as Research; } }

        public TextMeshProUGUI buttonText;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();

            buttonText.text = research.displayName;

            return result;
        }
    }
}

