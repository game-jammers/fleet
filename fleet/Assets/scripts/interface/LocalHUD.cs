//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;

namespace Fleet
{
    public class LocalHUD
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public IndustryData industry                            { get { return GameManager.instance.player.industry; } }

        public bool enableReserveList                           { get { return m_enableReserveList; } set { m_enableReserveList = value; Refresh(); } }
        private bool m_enableReserveList                        = true;

        public SpaceMenu menu;
        
        public UIList reserveList;
        public UIList enemyReserveList;
        public TextMeshProUGUI locationString;

        public TextMeshProUGUI researchName;
        public UISimpleMeter researchProgress;
        public TextMeshProUGUI manufactureName;
        public UISimpleMeter manufactureProgress;
        public TextMeshProUGUI fuelAmount;
        public UISimpleMeter fuelMeter;

        public UISimpleMeter dangerMeter;
        public UISimpleMeter waveMeter;
        public TextMeshProUGUI dangerLevel;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Initialize()
        {
            base.Initialize();
            menu.Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();

            FleetData fleet = GameManager.instance.player.fleet;

            menu.Refresh();
            
            if(enableReserveList)
            {
                reserveList.Show(0.5f, null);
                reserveList.SetData(fleet.vessels);
                enemyReserveList.Show(0.5f, null);
            }
            else
            {
                reserveList.Hide(0.5f, null);
                enemyReserveList.Hide(0.5f, null);
            }

            BaseSpaceSceneManager sm = BaseSpaceSceneManager.instance;
            locationString.text = sm.local.data.name;

            RefreshJobs();

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Refresh(BeltData belt)
        {
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //

        public void Refresh(PlanetData planet)
        {
            Refresh();
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        public void Refresh(StarData star)
        {
            Refresh();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void RefreshJobs()
        {
            IndustryData indy = industry;
            researchProgress.Hide(0f, null);
            manufactureProgress.Hide(0f, null);
            if(indy.manufactureJobs.Count > 0)
            {
                manufactureProgress.Show(0f, null);
                ManufactureJob job = indy.manufactureJobs[0];
                manufactureProgress.progress = job.perc;
                manufactureName.text = job.recipe.displayName;
            }

            if(indy.researchJobs.Count > 0)
            {
                researchProgress.Show(0f, null);
                ResearchJob job = indy.researchJobs[0];
                researchProgress.progress = job.perc;
                researchName.text = job.research.displayName;
            }

            FleetData.VesselEntry flagship = GameManager.instance.player.fleet.flagship;
            fuelMeter.progress = flagship.state.fuel / (float)flagship.data.stats.maxFuel;
            fuelAmount.text = System.String.Format("{0} / {1}", flagship.state.fuel, flagship.data.stats.maxFuel);

            BaseSpaceSceneManager sm = BaseSpaceSceneManager.instance;
            if(sm.solarSystem.isSol)
            {
                UniverseData udata = GameManager.instance.player.universe;
                dangerMeter.progress = udata.alienInstallationCount / (float)udata.maxAlienInstallations;
                waveMeter.progress = 1f;
                dangerLevel.text = System.String.Format("Infestations: {0}", udata.alienInstallationCount);
            }
            else
            {
                dangerMeter.progress = sm.local.waves.danger;
                waveMeter.progress = sm.local.waves.waveProgress;
                dangerLevel.text = System.String.Format("Danger Level: {0}", sm.local.waves.level);
            }

            enemyReserveList.SetData(sm.local.enemyVessels);
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void LateUpdate()
        {
           RefreshJobs();
        }
        
    }
}
