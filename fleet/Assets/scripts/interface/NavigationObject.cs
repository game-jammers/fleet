//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class NavigationObject
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public IStellarData stellarData                         { get; private set; }
        public UniverseData.Node universeData                   { get; private set; }
        public Image image;
        public TextMeshProUGUI nameText;
        public UIElement youAreHere;                            

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh(IStellarData data)
        {
            this.stellarData = data;
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //

        public void Refresh(UniverseData.Node data)
        {
            this.universeData = data;
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Refresh()
        {
            if(stellarData == null && universeData == null) return false;

            base.Refresh();

            if(stellarData != null)
            {
                image.sprite = stellarData.image;
                nameText.text = stellarData.name;
                if(stellarData.name == BaseSpaceSceneManager.instance.local.data.name)
                {
                    youAreHere.Show(0f, null);
                }
                else
                {
                    youAreHere.Hide(0f, null);
                }
            }
            else if(universeData != null)
            {
                image.sprite = universeData.image;
                nameText.text = universeData.id;
                if(universeData.id == BaseSpaceSceneManager.instance.solarSystem.name)
                {
                    youAreHere.Show(0f, null);
                }
                else
                {
                    youAreHere.Hide(0f, null);
                }
            }
            
            return true;
        }

        //
        // --------------------------------------------------------------------
        //

        public override void Select()
        {
            base.Select();
            if(stellarData != null)
            {
                BaseSpaceSceneManager.instance.local.hud.menu.navigation.Select(stellarData);
            }
            else
            {
                BaseSpaceSceneManager.instance.local.hud.menu.navigation.Select(universeData);
            }
        }
    }
}
