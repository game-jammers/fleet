//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class RecipeListDetail
        : UIListDetail
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public FleetData.VesselEntry flagship                   { get { return GameManager.instance.player.fleet.flagship; } }

        public new RecipeListItem item                          { get { return base.item as RecipeListItem; } }
        public Recipe recipe                                    { get { return item.recipe; } }

        public Color validColor;
        public Color invalidColor;

        public Image image;
        public TextMeshProUGUI title;
        public TextMeshProUGUI description;

        public TextMeshProUGUI inorganicCost;
        public TextMeshProUGUI organicCost;
        public TextMeshProUGUI exoticCost;
        public TextMeshProUGUI timeCost;
        public TextMeshProUGUI buildButtonText;

        private int buildCount                                  = 1;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            if(item != null)
            {
                image.sprite = recipe.image;
                title.text = recipe.displayName;
                description.text = recipe.description.text;

                int cost = recipe.cost[ResourceType.Inorganic].volume * buildCount;
                Color color = cost <= flagship.state.storage[ResourceType.Inorganic].volume ? validColor : invalidColor;
                inorganicCost.color = color;
                inorganicCost.text = cost.ToString("D4");

                cost = recipe.cost[ResourceType.Organic].volume * buildCount;
                color = cost <= flagship.state.storage[ResourceType.Organic].volume ? validColor : invalidColor;
                organicCost.color = color;
                organicCost.text = cost.ToString("D4");

                cost = recipe.cost[ResourceType.Exotic].volume * buildCount;
                color = cost <= flagship.state.storage[ResourceType.Exotic].volume ? validColor : invalidColor;
                exoticCost.color  = color;
                exoticCost.text = cost.ToString("D4");

                float time = recipe.buildTime * buildCount;
                timeCost.text = time.ToString("n1");

                buildButtonText.text = System.String.Format("Build x{0}", buildCount);
                return true;
            }

            return false;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Build()
        {
            GameManager.instance.player.industry.StartManufacture(recipe, buildCount);
            buildCount = 1;
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //

        public void IncBuildCount()
        {
            buildCount += 1;
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void DecBuildCount()
        {
            buildCount = btMath.Max(1, buildCount-1);
            Refresh();
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override void StartShow()
        {
            base.StartShow();
            buildCount = 1;
        }

        //
        // --------------------------------------------------------------------
        //

        protected IEnumerator RefreshAsync()
        {
            var wait = new WaitForSeconds(0.1f);
            while(true)
            {
                Refresh();
                yield return wait;
            }
        }
        

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            StartCoroutine(RefreshAsync());
        }
    }
}
