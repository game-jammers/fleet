//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class DataLogListDetail
        : UIListDetail
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public new DataLogListItem item                         { get { return base.item as DataLogListItem; } }
        public DataLog log                                      { get { return item.log; } }

        public Image image;
        public TextMeshProUGUI title;
        public TextMeshProUGUI description;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            if(item != null)
            {
                image.sprite = log.image;
                title.text = log.displayName;
                description.text = log.description.text;

                return true;
            }

            return false;
        }
    }        
}
