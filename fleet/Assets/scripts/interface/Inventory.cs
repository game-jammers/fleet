//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class Inventory
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public FleetData.VesselEntry flagship                   { get { return GameManager.instance.player.fleet.flagship; } }

        public UISimpleMeter capacity;
        public TextMeshProUGUI inorganic;
        public TextMeshProUGUI organic;
        public TextMeshProUGUI exotic;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            if(flagship != null)
            {
                capacity.SetValue(flagship.state.storage.spaceRemainingPerc);
                inorganic.text = flagship.state.storage[ResourceType.Inorganic].volume.ToString("D4");
                organic.text = flagship.state.storage[ResourceType.Organic].volume.ToString("D4");
                exotic.text = flagship.state.storage[ResourceType.Exotic].volume.ToString("D4");
            }

            return true;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator RefreshCoroutine()
        {
            var wait = new WaitForSeconds(0.1f);
            while(true)
            {
                Refresh();
                yield return wait;
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            StartCoroutine(RefreshCoroutine());
        }
    }
}
