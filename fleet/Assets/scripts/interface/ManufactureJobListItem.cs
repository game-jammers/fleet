//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class ManufactureJobListItem
        : UIListItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public ManufactureJob job                               { get { return data as ManufactureJob; } }

        public TextMeshProUGUI buttonText;
        public UISimpleMeter progressMeter;                  

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();

            buttonText.text = job.recipe.displayName;
            progressMeter.progress = job.perc;

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public void CancelJob()
        {
            GameManager.instance.player.industry.CancelJob(job);
        }
    }
}

