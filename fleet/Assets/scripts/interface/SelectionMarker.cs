//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class SelectionMarker
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Renderer render;
        public bool selected                                    { get { return m_selected; } set { m_selected = value; Refresh(); } }

        [ShowInInspector, ReadOnly] private bool m_selected     = false;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh()
        {
            render.enabled = m_selected;
        }
    }
}
