//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    [System.Serializable]
    public class IndustryData
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public List<Research> completeResearch                  = new List<Research>();
        public List<Research> availableResearch                 { get { return GetAvailableResearch(); } }

        public List<Recipe> unlockedRecipes                     { get { return GetUnlockedRecipes(); } }

        public List<DataLog> unlockedMechanicLogs               { get { return GetUnlockedLogs(DataLog.Type.Mechanic); } }
        public List<DataLog> unlockedLoreLogs                   { get { return GetUnlockedLogs(DataLog.Type.Lore); } }

        public List<ManufactureJob> manufactureJobs             = new List<ManufactureJob>();
        public List<ResearchJob> researchJobs                   = new List<ResearchJob>();

        //
        // init ///////////////////////////////////////////////////////////////
        //

        public virtual void Initialize()
        {
            IndustryDb industry = Database.GetTable<IndustryDb>();
            completeResearch.AddRange(industry.GetDefaultResearch());
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Update(float dt)
        {
                #if UNITY_EDITOR
                if(GameManager.instance.verboseDebug)
                {
                    Dbg.Log("\tIndustry Updating", dt);
                }
                #endif

            if(manufactureJobs.Count > 0)
            {
                manufactureJobs[0].Update(dt);
                if(manufactureJobs[0].remaining <= 0f)
                {
                    manufactureJobs.Pop();
                }
            }

            if(researchJobs.Count > 0)
            {
                #if UNITY_EDITOR
                if(GameManager.instance.verboseDebug)
                {
                    Dbg.Log("\t\tResearch Job Updating: {0}", dt);
                }
                #endif

                researchJobs[0].Update(dt);
                if(researchJobs[0].remaining <= 0f)
                {
                    researchJobs.Pop();
                }
            }
        }

        //
        // manufacture ////////////////////////////////////////////////////////
        //
       
        public ManufactureJob StartManufacture(Recipe recipe, int buildCount)
        {
            ManufactureJob job = new ManufactureJob(recipe, buildCount, OnManufactureJobComplete);
            if(job.CanBuild(buildCount))
            {
                job.Start();
                manufactureJobs.Add(job);
                BaseSpaceSceneManager.instance.local.hud.Refresh();
                return job;
            }
            return null;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public ManufactureJob GetActiveJob(Recipe recipe)
        {
            return manufactureJobs.Find(j=>j.recipe ==recipe);
        }

        //
        // --------------------------------------------------------------------
        //

        public void CancelJob(ManufactureJob job)
        {
            manufactureJobs.RemoveAll(j=>j == job);
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnManufactureJobComplete(ManufactureJob job)
        {
        }

        //
        // --------------------------------------------------------------------
        //

        public ResearchJob StartResearch(Research research)
        {
            if(availableResearch.IndexOf(research) < 0) return null;
        
            ResearchJob job = new ResearchJob(research, OnResearchJobComplete);
            job.Start();
            researchJobs.Add(job);
            BaseSpaceSceneManager.instance.local.hud.Refresh();
            return job;
        }

        //
        // --------------------------------------------------------------------
        //

        public void CancelJob(ResearchJob job)
        {
            researchJobs.RemoveAll(j=>j==job);
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnResearchJobComplete(ResearchJob job)
        {
            completeResearch.Add(job.research);
        }     

        //
        // private methods ////////////////////////////////////////////////////
        //

        private List<Research> GetAvailableResearch()
        {
            List<Research> result = new List<Research>();
            IndustryDb industry = Database.GetTable<IndustryDb>();
            foreach(Research res in industry.research)
            {
                // did we already finish it?
                if(completeResearch.Exists(ar=>ar == res) == false)
                {
                    // are we currently working on it?
                    if(researchJobs.Find(j=>j.research == res) == null)
                    {
                        // is it unlocked?
                        if(res.isUnlocked(completeResearch))
                        {
                            result.Add(res);
                        }
                    }
                }
            }
            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public List<Recipe> GetUnlockedRecipes()
        {
            List<Recipe> result = new List<Recipe>();
            foreach(Research complete in completeResearch)
            {
                result.AddRange(complete.unlocksRecipes);
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public List<DataLog> GetUnlockedLogs(DataLog.Type type)
        {
            List<DataLog> result = new List<DataLog>();
            foreach(Research complete in completeResearch)
            {
                result.AddRange(complete.unlocksLogs.FindAll(l=>l.type == type));
            }

            return result;
        }

    }
}
