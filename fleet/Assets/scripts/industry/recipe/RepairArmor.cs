//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;

namespace Fleet
{
    public class RepairArmor
        : RecipeLogic
    {
        //
        // create /////////////////////////////////////////////////////////////
        //

        public RepairArmor(Recipe rec)
            : base(rec)
        {
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Complete(ManufactureJob job)
        {
            FleetData fleet = GameManager.instance.player.fleet;
            foreach(FleetData.VesselEntry entry in fleet.entries)
            {
                int amount = (int)btMath.Round(entry.data.stats.maxArmor * 0.1f) * job.buildCount;
                entry.state.armor = btMath.Min(entry.state.armor + amount, entry.data.stats.maxArmor);
            }
        }
    }
}
