//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public abstract class RecipeLogic
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Recipe recipe                                    { get; private set; }
        public FleetData.VesselEntry flagship                   { get { return GameManager.instance.player.fleet.flagship; } }
        
        //
        // create /////////////////////////////////////////////////////////////
        //

        public RecipeLogic(Recipe recipe)
        {
            this.recipe = recipe;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual void Begin(ManufactureJob job)
        {
            flagship.state.storage.Remove(job.totalCost);
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Complete(ManufactureJob job)
        {
        }
    }
}
