//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;

namespace Fleet
{
    public class JumpFuel
        : RecipeLogic
    {
        //
        // create /////////////////////////////////////////////////////////////
        //

        public JumpFuel(Recipe rec)
            : base(rec)
        {
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Complete(ManufactureJob job)
        {
            flagship.state.fuel += job.buildCount;
        }
    }
}
