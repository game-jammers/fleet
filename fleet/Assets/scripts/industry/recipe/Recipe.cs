//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    [CreateAssetMenu(fileName="Recipe", menuName="Fleet/Recipe")]
    public class Recipe
        : ScriptableObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Sprite image;
        public string displayName;
        public TextAsset description;
        public float buildTime;

        [TypeStr(typeof(RecipeLogic))] public string logicType;
        public RecipeLogic logic                                { get { return GetLogic(); } }
        private RecipeLogic m_logic                             = null;
        
        public bool requireShipyard                             = false;
        public int inorganicCost;
        public int organicCost;
        public int exoticCost;

        public ResourceList cost                                { get { return GetCost(); } }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private RecipeLogic GetLogic()
        {
            if(m_logic == null && System.String.IsNullOrEmpty(logicType) == false)
            {
                m_logic = System.Activator.CreateInstance(System.Type.GetType(logicType), new System.Object[] { this }) as RecipeLogic;
            }
            return m_logic;
        }

        //
        // --------------------------------------------------------------------
        //

        private ResourceList GetCost()
        {
            ResourceList res = new ResourceList();
            res[ResourceType.Inorganic] = new Resource(ResourceType.Inorganic, inorganicCost);
            res[ResourceType.Organic] = new Resource(ResourceType.Organic, organicCost);
            res[ResourceType.Exotic] = new Resource(ResourceType.Exotic, exoticCost);
            return res;
        } 

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnEnable()
        {
            GetLogic();
        }
    }
}
