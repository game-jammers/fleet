//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;

namespace Fleet
{
    public class RepairHull
        : RecipeLogic
    {
        //
        // create /////////////////////////////////////////////////////////////
        //

        public RepairHull(Recipe rec)
            : base(rec)
        {
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Complete(ManufactureJob job)
        {
            FleetData fleet = GameManager.instance.player.fleet;
            foreach(FleetData.VesselEntry entry in fleet.entries)
            {
                int amount = (int)btMath.Round(entry.data.stats.maxHull * 0.1f) * job.buildCount;
                entry.state.hull = btMath.Min(entry.state.hull + amount, entry.data.stats.maxHull);
            }
        }
    }
}
