//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;

namespace Fleet
{
    public class ShipConstruction
        : RecipeLogic
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public VesselData vesselData                            { get; private set; }
        
        //
        // create /////////////////////////////////////////////////////////////
        //

        public ShipConstruction(Recipe rec)
            : base(rec)
        {
            FleetDb db = Database.GetTable<FleetDb>();
            VesselData[] vessels = db.FindVessel(v=>v.name == rec.name);
            Dbg.Assert(vessels.Length == 1, "{0} found {1} vessel data which is not 1", rec.name, vessels.Length);
            vesselData = vessels[0];
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Complete(ManufactureJob job)
        {
            FleetDb fdb = Database.GetTable<FleetDb>();
            for(int i = 0; i < job.buildCount; ++i)
            {
                GameManager.instance.player.fleet.AddVessel(recipe.name, vesselData);
            }
            BaseSpaceSceneManager.instance.local.hud.Refresh();
        }
    }
}
