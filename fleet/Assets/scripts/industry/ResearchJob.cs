//
// (c) GameJammers 2021
// http://www.jamming.games
//

namespace Fleet
{
    public class ResearchJob
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Research research;
        public float required                                   { get { return research.difficulty; } }
        public float remaining                                  { get { return required - progress; } }
        public float perc                                       { get { return progress / required; } }
        public float progress                                   { get; private set; }

        private System.Action<ResearchJob> callback;

        //
        // create /////////////////////////////////////////////////////////////
        //

        public ResearchJob(Research research, System.Action<ResearchJob> cb)
        {
            this.research = research;
            progress = 0f;
            callback = cb;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Start()
        {
            if(required <= 0f)
            {
                Complete();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Update(float dt)
        {
            progress += dt;
            if( perc >= 1f)
            {
                Complete();
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void Complete()
        {
            if(callback != null)
            {
                callback(this);
            }
        }
    }
}
