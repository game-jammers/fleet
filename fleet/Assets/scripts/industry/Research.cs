//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    [CreateAssetMenu(fileName="Research", menuName="Fleet/Research")]
    public class Research
        : ScriptableObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Sprite image;
        public string displayName;
        public TextAsset description;
        public float difficulty;
        public Research[] requiresResearch;
        public Recipe[] unlocksRecipes;
        public DataLog[] unlocksLogs;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public bool isUnlocked(List<Research> complete)
        {
            foreach(Research req in requiresResearch)
            {
                if(!complete.Contains(req))
                {
                    return false; 
                }
            }
            return true;
        }
    }
}
