//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class ManufactureJob
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Recipe recipe                                    { get; private set; }
        public FleetData.VesselEntry flagship                   { get { return GameManager.instance.player.fleet.flagship; } }

        public int buildCount                                   { get; private set; }
        public float progress                                   { get; private set; }
        public float totalBuildTime                             { get { return recipe.buildTime * buildCount; } }
        public float remaining                                  { get { return totalBuildTime - progress; } }
        public float perc                                       { get { return totalBuildTime > 0f ? progress / totalBuildTime : 1f; } }

        public ResourceList totalCost                           { get { return GetTotalCost(); } }
        public int maxBuildCount                                { get { return GetMaxBuildCount(); } }

        private System.Action<ManufactureJob> callback;

        //
        // create /////////////////////////////////////////////////////////////
        //

        public ManufactureJob(Recipe recipe, int count, System.Action<ManufactureJob> cb)
        {
            this.recipe = recipe;
            buildCount = count;
            progress = 0f;
            callback = cb;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Start()
        {
            if(recipe.logic != null)
            {
                recipe.logic.Begin(this);
            }

            if(totalBuildTime <= 0f)
            {
                Complete();
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update(float dt)
        {
            progress += dt;
            if(perc >= 1f)
            {
                Complete();
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public bool CanBuild(int buildCount)
        {
            var sm = BaseSpaceSceneManager.instance;
            bool hasShipyard = sm.local.vessels.IndexOf(v=>v.data.type == VesselType.Shipyard) >= 0;
            return maxBuildCount >= buildCount && ( !recipe.requireShipyard || hasShipyard);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void Complete()
        {
            if(recipe.logic != null)
            {
                recipe.logic.Complete(this);
            }
            if(callback != null)
            {
                callback(this);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private ResourceList GetTotalCost()
        {
            ResourceList result = new ResourceList(int.MaxValue);
            foreach(Resource res in recipe.cost)
            {
                result[res.type] = new Resource() {
                    type = res.type,
                    volume = res.volume * buildCount
                };
            }
            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        private int GetMaxBuildCount()
        {
            int max = flagship.state.storage.Max( recipe.cost );
            return btMath.Min(flagship.data.stats.maxFuel - flagship.state.fuel, max);
        }
    }
}
