//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    [CreateAssetMenu(fileName="DataLog", menuName="Fleet/DataLog")]
    public class DataLog
        : ScriptableObject
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public enum Type
        {
            Mechanic,
            Lore
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        public Sprite image;
        public string displayName;
        public Type type;
        public TextAsset description;
    }
}
