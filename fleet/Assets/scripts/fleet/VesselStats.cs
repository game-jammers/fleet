//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    [System.Serializable]
    public struct VesselStats
    {
        [Header("Transit")]
        public float size;
        public float jumpWell;
        public float jumpSpeed;
        public int jumpCost;

        public float acceleration_mps;
        public float maxSpeed_mps;
        public float turnSpeedDeg;

        [Header("Mining")]
        public float miningDist;
        public float inorganicMiningMult;
        public float organicMiningMult;
        public float exoticMiningMult;
        public WeaponFx miningFx;

        [Header("Offense")]
        public WeaponData[] weapons;
        public float targetRange                                { get { return GetTargetRange(); } }

        [Header("Defenses")]
        public float shieldRecharge;
        public float maxShield;
        public float maxArmor;
        public float maxHull;
        public int maxFuel;

        [Header("Storage")]
        public int storageCapacity;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public float GetMiningMult(ResourceType type)
        {
            switch(type)
            {
                case ResourceType.Inorganic: return inorganicMiningMult;
                case ResourceType.Organic: return organicMiningMult;
                case ResourceType.Exotic: return exoticMiningMult;
            }

            throw new System.NotImplementedException(System.String.Format("Unknown Resource Type {0}", type));
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private float GetTargetRange()
        {
            float result = float.PositiveInfinity;
            foreach(WeaponData weap in weapons)
            {
                result = btMath.Min(result, weap.range);
            }
            return result;
        }
    }
}
