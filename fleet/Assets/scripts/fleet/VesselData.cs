//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public enum Faction
    {
        Human,
        Mythos
    }

    //
    // ########################################################################
    //

    public enum VesselType
    {
        Fighter,
        Cruiser,
        Battleship,
        Recon,
        MiningBarge,
        MiningDrone,
        Turret,
        Shipyard
    }

    //
    // ########################################################################
    //
    
    [CreateAssetMenu(fileName="VesselData", menuName="Fleet/VesselData")]
    public class VesselData
        : ScriptableObject
    {
        public VesselType type;
        public Faction faction;
        [EnumFlag(typeof(VesselCap))] public int capabilities;
        public List<VesselCap> caps                             { get { return EnumUtility.FlagsInValue<VesselCap>(capabilities); } }
        public Sprite icon;
        public VesselView view;
        public VesselStats stats;
        public GameObject deathFx;
    }
}
