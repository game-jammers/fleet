//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class VesselView
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Vessel vessel                                    { get; private set; }
        public GameObject transitFxPrefab;
        public GameObject mesh;
        public Quaternion offsetRot                             = Quaternion.identity;

        private GameObject transitFx                            = null;

        //
        // init ///////////////////////////////////////////////////////////////
        //

        public virtual void Initialize(Vessel vessel)
        {
            this.vessel = vessel;
            offsetRot = transform.localRotation;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(vessel == null) return;

            switch(vessel.state.mode)
            {
                case VesselState.Mode.Reserve:
                {
                    if(transitFx != null)
                    {
                        Destroy(transitFx.gameObject);
                        transitFx = null;
                    }

                    mesh.SetActive(false);
                }
                break;

                case VesselState.Mode.TransitEnter:
                case VesselState.Mode.TransitExit:
                {
                    mesh.SetActive(false);
                    if(transitFx == null)
                    {
                        transitFx = Instantiate(transitFxPrefab, transform.position, transform.rotation);
                        transitFx.transform.SetParent(transform);
                    }
                }
                break;

                case VesselState.Mode.Deployed:
                {
                    mesh.SetActive(true);
                    if(transitFx != null)
                    {
                        Destroy(transitFx.gameObject);
                        transitFx = null;
                    }
                }
                break;
            }
        }
    }
}
