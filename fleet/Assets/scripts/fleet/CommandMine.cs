//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{ 
    public class CommandMine
        : CommandMove
    {

        //
        // members ////////////////////////////////////////////////////////////
        //

        public Asteroid asteroid;
        public ResourceType resource;
        public override Vector3 target                          { get { return GetTarget(); } }
        public float cycleTime                                  { get; private set; }

        private float targetDist                                { get { return owner.data.stats.miningDist * 0.9f; } }
        private bool isMining                                   = false;
        private float lastTick                                  = 0f;
        private WeaponFx miningFx                               = null;

        //
        // create /////////////////////////////////////////////////////////////
        //
        
        public CommandMine(Vessel owner, Asteroid asteroid, ResourceType resource)
            : base(owner, asteroid.transform.position)
        {
            this.resource = resource;
            this.asteroid = asteroid;
            isMining = false;
            lastTick = 0f;
            cycleTime = 1f * owner.data.stats.GetMiningMult(resource);
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Update(float dt)
        {
            Vector3 diff = target - owner.transform.position;
            Vector3 dir = diff.normalized;
            float dist2 = diff.sqrMagnitude;

            bool finished = false;

            // if we are asked to stop use regular movemet logic until we are done
            if(stopping)
            {
                StopMining();
                finished = base.Update(dt);
            }

            // if we are in range to mine, eat it up
            else if(dist2 <= targetDist * targetDist)
            {
                if(!isMining)
                {
                    StartMining();
                }

                if(Time.time - lastTick > cycleTime)
                {
                    lastTick = Time.time;
                    finished = OnMiningTick();
                }
            }

            // otherwise move towards the object
            else 
            {
                StopMining();
                base.Update(dt);
            }

            return finished;
        }

        //
        // --------------------------------------------------------------------
        //

        public override void End()
        {
            StopMining();
        }
        

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void StartMining()
        {
            isMining = true;
            lastTick = Time.time;
            miningFx = WeaponFx.Create(owner, asteroid, owner.data.stats.miningFx);
        }

        //
        // --------------------------------------------------------------------
        //

        private void StopMining()
        {
            isMining = false;
            lastTick = Time.time;
            if(miningFx != null)
            {
                GameObject.Destroy(miningFx.gameObject);
                miningFx = null;
            }
        }
        
        //
        // --------------------------------------------------------------------
        //

        private bool OnMiningTick()
        {
            int remain = asteroid.data.GetResourceRemaining(resource);

            VesselState flagship = GameManager.instance.player.fleet.flagship.state;
            if(flagship.storage.spaceRemaining > 0 && remain > 0)
            {
                Resource res = new Resource() {
                    type = resource,
                    volume = 1
                };

                asteroid.data.Remove(res);
                flagship.storage.Add(res);
            }

            return flagship.storage.spaceRemaining == 0
                || asteroid.data.GetResourceRemaining(resource) == 0;
        }

        //
        // --------------------------------------------------------------------
        //

        private Vector3 GetTarget()
        {
            Vector3 diff = asteroid.transform.position - owner.transform.position;
            Vector3 dir = diff.normalized;

            return asteroid.transform.position - (dir * targetDist);
        }
    }
}
