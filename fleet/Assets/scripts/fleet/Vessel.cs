//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public enum ControllerType
    {
        Player,
        AI
    }

    //
    // ########################################################################
    //
    
    public class Vessel
        : SpaceObject
    {
        //
        // events /////////////////////////////////////////////////////////////
        //

        public delegate void VesselDestroyedCallback(Vessel vessel);
        public event VesselDestroyedCallback OnVesselDestroyed;
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public virtual VesselData data                          { get; protected set; }
        public virtual VesselView view                          { get; protected set; }
        public override float jumpWell                          { get { return data.stats.jumpWell; } }
        public override float visualSize                        { get { return data.stats.size; } }
        [ReadOnly] public VesselState state;
        public ControllerType controller                        = ControllerType.Player;
        public BaseSpaceSceneManager sm                         { get { return BaseSpaceSceneManager.instance; } }

        public bool cutsceneBlock                               = false;
        public bool debugAi                                     = false;

        public BaseCommand activeCommand                        = null;
        private BaseCommand nextCommand                         = null;
        private bool exploded                                   = false;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static Vessel JumpIn(Vessel prefab, VesselData data, VesselState state, Xform target)
        {
            if(data.stats.jumpCost > state.fuel) return null;

            Vessel vessel = Instantiate(prefab, target.position, target.rotation);
            vessel.Initialize(data, state, target);

            // turrets are placed like normal vessels at first, but never jump out
            // never move, and are no longer part of your "fleet" but part of your "statics"
            if(data.type == VesselType.Turret || data.type == VesselType.Shipyard)
            {
                FleetData fleet = GameManager.instance.player.fleet;
                fleet.vessels.RemoveAll(v=>v.state == state);
                fleet.statics.Add(new FleetData.StaticEntry() {
                    vessel = new FleetData.VesselEntry() {
                        data = data,
                        state = vessel.state
                    },
                    location = vessel.sm.local.data,
                    xform = target,
                    beseiged = false
                });
                vessel.sm.local.hud.Refresh();
            }

            return vessel;
        }

        //
        // --------------------------------------------------------------------
        //

        public static Vessel Emplace(Vessel prefab, FleetData.StaticEntry entry)
        {
            Vessel vessel = Instantiate(prefab, entry.xform.position, entry.xform.rotation);
            vessel.Initialize(entry.vessel.data, entry.vessel.state, entry.xform);
            vessel.state.mode = VesselState.Mode.Deployed;
            return vessel;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Initialize(VesselData data, VesselState state, Xform target)
        {
            this.name = state.name;
            this.data = data;

            view = Instantiate(data.view, transform.position, transform.rotation);
            view.transform.SetParent(transform);
            view.Initialize(this);

            this.state = state;
            this.state.mode = VesselState.Mode.TransitEnter;
            this.state.transitAmt = 0f;
            this.state.transitTarget = target;

            switch(data.type)
            {
                case VesselType.Fighter: this.state.ai = AIType.Aggressive; break;
                case VesselType.Cruiser: this.state.ai = AIType.Aggressive; break;
                case VesselType.Battleship: this.state.ai = AIType.Aggressive; break;
                case VesselType.Recon: this.state.ai = AIType.Defensive; break;
                case VesselType.MiningBarge: this.state.ai = AIType.Coward; break;
                case VesselType.MiningDrone: this.state.ai = AIType.Coward; break;
                case VesselType.Turret: this.state.ai = AIType.Aggressive; break;
                case VesselType.Shipyard: this.state.ai = AIType.Aggressive; break;
            }
            this.exploded = false;
        }

        //
        // --------------------------------------------------------------------
        //

        public void JumpOut()
        {
            if(data.type == VesselType.Turret) return;
            if(data.type == VesselType.Shipyard) return;
            if(state.mode != VesselState.Mode.Reserve)
            {
                state.transitAmt = 0f;
                state.mode = VesselState.Mode.TransitExit;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void TakeDamage(Vessel attacker, WeaponData weapon)
        {
            float damage = weapon.damage;
            if(state.mode == VesselState.Mode.TransitEnter || state.mode == VesselState.Mode.TransitExit)
            {
                damage = damage / 2f;
            }

            if(damage > 0)
            {
                float delt = Mathf.Min(state.shields, damage);
                state.shields = btMath.Max(0, state.shields - delt);
                if(weapon.damageType == DamageType.Laser)
                    state.shields = btMath.Max(0, state.shields - delt);

                damage = btMath.Max(0, damage-delt);
            }

            if(damage > 0)
            {
                float delt = btMath.Min(state.armor, damage);
                state.armor = btMath.Max(0, state.armor - delt);
                if(weapon.damageType == DamageType.Projectile)
                    state.armor = btMath.Max(0, state.armor - delt);

                damage = btMath.Max(0, damage-delt);
            }

            if(damage > 0)
            {
                float delt = Mathf.Min(state.hull, damage);
                state.hull = btMath.Max(0, state.hull - delt);
            }

            if(state.ai == AIType.Defensive || state.ai == AIType.Reactive)
            {
                if(activeCommand == null || activeCommand is CommandMine)
                {
                    Attack(attacker);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Explode()
        {
            if(!exploded)
            {
                BaseSpaceSceneManager sm = BaseSpaceSceneManager.instance;
                if(GameManager.instance.player.fleet.flagship.state == state)
                {
                    sm.local.EndGame();
                }
                else if(controller == ControllerType.Player)
                {
                    GameManager.instance.player.fleet.OnVesselDestroyed(this);
                    sm.local.hud.Refresh();
                }
                else if(sm.solarSystem.isSol && controller == ControllerType.AI)
                {
                    switch(data.type)
                    {
                        case VesselType.Fighter: sm.solarSystem.universeData.danger -= 0.1f; break;
                        case VesselType.Cruiser: sm.solarSystem.universeData.danger -= 0.2f; break;
                        case VesselType.Battleship: sm.solarSystem.universeData.danger -= 2f; break;
                    }
                }

                Instantiate(data.deathFx, transform.position, transform.rotation);
                Destroy(gameObject);
                exploded = true;
            }
        }
        

        //
        // commands ///////////////////////////////////////////////////////////
        //

        private void SetCommand(BaseCommand next)
        {
            if(activeCommand != null)
            {
                activeCommand.Interrupt(next);
            }

            nextCommand = next;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Attack(Vessel other)
        {
            if(this != null && other != null)
            {
                SetCommand( new CommandAttack(this, other) );
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void MoveTo(Vector3 position)
        {
            if(data.type == VesselType.Turret || data.type == VesselType.Shipyard) return;
            SetCommand( new CommandMove(this, position) );
        }

        //
        // --------------------------------------------------------------------
        //

        public void Mine(ResourceType type, Asteroid a)
        {
            SetCommand( new CommandMine(this, a, type) );
        }

        //
        // --------------------------------------------------------------------
        //

        public void ClearCommand()
        {
            SetCommand(null);
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override InspectData GetInspectData()
        {
            throw new System.NotImplementedException("Should not inspect vessels any longer");
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void UpdateCommand(float dt)
        {
            if(activeCommand != null)
            {
                if(activeCommand.Update(dt))
                { 
                    activeCommand.End();
                    activeCommand = null;
                }
            }

            if(activeCommand == null)
            {
                if(nextCommand != null)
                {
                    nextCommand.Start();
                    activeCommand = nextCommand;
                    nextCommand = null;
                }
                else
                {
                    switch(state.ai)
                    {
                        case AIType.Reactive: break; // handled by taking damage
                        case AIType.Passive: break;
                        case AIType.Aggressive: SetCommand(new CommandAttack(this, null)); break;
                        //case AIType.Defensive: Dbg.Warn("{0} is Defensive", name); break;
                        //case AIType.Coward: Dbg.Warn("{0} is Defensive", name); break;
                    }
                }
            }
        }

        #if UNITY_EDITOR
        [UnityEngine.ContextMenu("Sabotage")]
        public void Sabotage()
        {
            state.shields = 0;
            state.armor = 0;
            state.hull = 1;
        }
        #endif

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(SceneManager.instance.pause != PauseMode.None) return;
            if(cutsceneBlock) return;
            if(!state.alive)
            {
                Explode();
                return;
            }

            switch(state.mode)
            {
                case VesselState.Mode.Reserve:
                {
                }
                break;

                case VesselState.Mode.TransitEnter:
                {
                    state.transitAmt += Time.deltaTime;
                    if(state.transitAmt > data.stats.jumpSpeed)
                    {
                        state.transitAmt = 0f;
                        state.mode = VesselState.Mode.Deployed;
                    }
                }
                break;

                case VesselState.Mode.TransitExit:
                {
                    if(state.fuel < data.stats.jumpCost)
                    {
                        state.mode = VesselState.Mode.Deployed;
                    }
                    else
                    {
                        state.transitAmt += Time.deltaTime;
                        if(state.transitAmt > data.stats.jumpSpeed)
                        {
                            state.transitAmt = 0f;
                            state.mode = VesselState.Mode.Reserve;
                            state.fuel -= data.stats.jumpCost;
                            Destroy(gameObject);
                        }
                    }
                }
                break;

                case VesselState.Mode.Deployed:
                {
                    UpdateCommand(Time.deltaTime);
                }
                break;
            }

            //
            // shield tick 
            //
            if(state.shields < data.stats.maxShield)
            {
                state.shields = btMath.Min(data.stats.maxShield, state.shields + (data.stats.shieldRecharge * Time.deltaTime));
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void LateUpdate()
        {
            if(activeCommand == null)
            {
                foreach(SpaceObject so in sm.local.spaceObjects)
                {
                    Vector3 diff = so.transform.position - transform.position;
                    if(diff.sqrMagnitude < 50f)
                    {
                        transform.position += -diff.normalized * 1f * Time.deltaTime;
                    }
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        #if UNITY_EDITOR
        protected virtual void OnGUI()
        {
            if(debugAi)
            {
                GUILayout.Label(name);
                GUILayout.Label(System.String.Format("Mode: {0}", state.mode));
                if(activeCommand != null)
                {
                    GUILayout.Label(System.String.Format("Command: {0}", activeCommand.GetType()));
                    GUILayout.Label(activeCommand.ToString());
                }
            }
        }
        #endif

        //
        // --------------------------------------------------------------------
        //

        protected override void OnDestroy()
        {
            base.OnDestroy();
            VesselDestroyedCallback cb = OnVesselDestroyed;
            if(cb != null)
            {
                cb(this);
            }
        }
        
    }
}
