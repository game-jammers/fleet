//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

#if UNITY_EDITOR
//    #define FULL_FLEET
//    #define SCROOGE_MCDUCK
#endif

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class FleetData
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public class VesselEntry
        {
            public VesselData data;
            public VesselState state;
        }

        //
        // --------------------------------------------------------------------
        //

        public class StaticEntry
        {
            public VesselEntry vessel;
            public IStellarData location;
            public Xform xform;
            public bool beseiged;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        public List<VesselEntry> vessels                        = new List<VesselEntry>();
        public List<StaticEntry> statics                        = new List<StaticEntry>();
        public IEnumerable<VesselEntry> entries                 { get { return GetEntries(); } }
        public VesselEntry flagship                             { get; private set; }

        //
        // init ///////////////////////////////////////////////////////////////
        //
        
        public void Initialize()
        {
            if(vessels.Count == 0)
            {
                FleetDb fleetdb = Database.GetTable<FleetDb>();
                VesselData[] flagships = fleetdb.FindVessel(Faction.Human, (v)=>v.type == VesselType.Recon);
                Dbg.Assert(flagships.Length == 1, "Found the wrong number of flagships? {0}", flagships.Length);
                flagship = AddVessel("Flagship", flagships[0]);

                #if FULL_FLEET
                    foreach(VesselData vd in fleetdb.humanVessels)
                    {
                        if(vd.type != VesselType.Recon)
                        {
                            AddVessel(vd.name+'1', vd);
                            AddVessel(vd.name+'2', vd);
                        }
                    }
                #endif

                #if SCROOGE_MCDUCK
                    flagship.state.storage = new ResourceList(99999);
                    flagship.state.storage[ResourceType.Inorganic] = new Resource(ResourceType.Inorganic, 9999);
                    flagship.state.storage[ResourceType.Organic] = new Resource(ResourceType.Organic, 9999);
                    flagship.state.storage[ResourceType.Exotic] = new Resource(ResourceType.Exotic, 9999);
                #endif
            }
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Update(float dt)
        {
            //noop
        }

        //
        // --------------------------------------------------------------------
        //

        public VesselEntry AddVessel(string name, VesselData data)
        {
            VesselEntry entry = new VesselEntry() {
                data = data,
                state = VesselState.Create(name, data)
            };
            
            vessels.Add(entry);

            return entry;
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnVesselDestroyed(Vessel vessel)
        {
            vessels.RemoveAll(e=>e.state == vessel.state);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerable<VesselEntry> GetEntries()
        {
            foreach(VesselEntry entry in vessels)
                yield return entry;

            foreach(StaticEntry sentry in statics)
                yield return sentry.vessel;
        }
        
    }
}
