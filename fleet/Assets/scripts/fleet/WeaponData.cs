//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public enum DamageType
    {
        Neutral,    //no bonus
        Projectile, //bonus to armor
        Laser,      //bonus to shield
    }

    //
    // ########################################################################
    //

    [CreateAssetMenu(fileName="Weapon", menuName="Fleet/Weapon")]
    public class WeaponData
        : ScriptableObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public float range;
        public DamageType damageType;
        public bool directDamage;
        public float damage;
        public WeaponFx fx;
        public float shotsPerSec;
        public float shotDelaySec                               { get { return 1f / shotsPerSec; } }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static void Fire(Vessel shooter, Vessel target, WeaponData weapon)
        {
            Dbg.Assert(shooter != null, "Cannot fire a weapon without a shooter");
            Dbg.Assert(target != null, "Cannot fire a weapon without a target");
            Dbg.Assert(weapon != null, "Cannot fire a weapon without a weapon");

            WeaponFx newFx = WeaponFx.Create(shooter, target, weapon);
            if(weapon.directDamage)
            {
                target.TakeDamage(shooter, weapon);
            }
        }
    }
}
