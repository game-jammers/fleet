//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;

namespace Fleet
{
    [System.Serializable]
    public class PlayerData
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public int seed                                         = 0;
        public System.Random rng                                = null;
        public UniverseData universe                            = null;
        public FleetData fleet                                  = new FleetData();
        public IndustryData industry                            = new IndustryData();

        //
        // init ///////////////////////////////////////////////////////////////
        //

        public virtual void Initialize()
        {
            if(rng == null)
            {
                seed = btRandom.RandomInt();
                rng = new System.Random();
                universe = UniverseData.Create(rng.Next(), rng.Range(10,25));
                fleet.Initialize();
                industry.Initialize();
            }
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual void Update(float dt)
        {
            fleet.Update(dt);
            #if UNITY_EDITOR
                if(GameManager.instance.verboseDebug)
                {
                    Dbg.Log("Flagship Mode: {0}", fleet.flagship.state.mode.ToString());
                }
            #endif
 
            if(fleet.flagship.state.mode == VesselState.Mode.Deployed)
            {
                industry.Update(dt);
            }
        }
        
    }
}
