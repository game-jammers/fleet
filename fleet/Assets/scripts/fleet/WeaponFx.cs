//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class WeaponFx
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vessel shooter                                   { get; private set; }
        public SpaceObject target                               { get; private set; }
        public WeaponData weapon                                { get; private set; }
        
        //
        // create /////////////////////////////////////////////////////////////
        //

        public static WeaponFx Create(Vessel shooter, SpaceObject target, WeaponData weapon)
        {
            WeaponFx result = Instantiate(weapon.fx, shooter.transform.position, shooter.transform.rotation);
            result.Initialize(shooter, target, weapon);
            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static WeaponFx Create(Vessel shooter, SpaceObject target, WeaponFx fx)
        {
            WeaponFx result = Instantiate(fx, shooter.transform.position, shooter.transform.rotation);
            result.Initialize(shooter, target, null);
            return result;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual void Initialize(Vessel shooter, SpaceObject target, WeaponData weapon)
        {
            this.shooter = shooter;
            this.target = target;
            this.weapon = weapon;
        }
    }
}
