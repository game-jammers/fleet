//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class CommandAttack
        : CommandMove
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        private class WeaponState
        {
            public float lastFired;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vessel attackTarget                              { get; protected set; }
        public override Vector3 target                          { get { return GetTarget(); } }

        private float targetDist                                { get { return owner.data.stats.targetRange * 0.8f; } }
        private Dictionary<WeaponData, WeaponState> wstates     = new Dictionary<WeaponData, WeaponState>();

        //
        // create /////////////////////////////////////////////////////////////
        //
        
        public CommandAttack(Vessel owner, Vessel attackTarget)
            : base(owner, owner.transform.position)
        {
            this.attackTarget = attackTarget;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Update(float dt)
        {
            bool finished = false;

            // request stopping 
            if(stopping)
            {
                finished = base.Update(dt);
            }

            // we have no target
            else if(attackTarget == null)
            {
                if(owner.state.ai == AIType.Aggressive)
                {
                    attackTarget = GetClosestTarget();
                }
                else
                {
                    finished = true;
                }
            }

            // we have a target ...
            else 
            {
                Vector3 diff = attackTarget.transform.position - owner.transform.position;
                float dist2 = diff.sqrMagnitude;
                if(dist2 < targetDist * targetDist)
                {
                    TurnTowards(owner, target, dt);
                    AttackUpdate(dt);
                }
                else
                {
                    base.Update(dt);
                }
            }

            return finished;
        }

        //
        // ////////////////////////////////////////////////////////////////////
        //
        
        public override string ToString()
        {
            return System.String.Format(
                "AttackTarget: {0}\nTarget: {1}\nTargetDist: {2}\n",
                attackTarget.name,
                target.ToString(),
                targetDist
            );
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private Vessel GetClosestTarget()
        {
            float closestDist2 = float.PositiveInfinity;
            Vessel closest = null;
            foreach(Vessel other in BaseSpaceSceneManager.instance.local.vessels)
            {
                if(other.controller != owner.controller)
                {
                    if(!owner.state.dontAttackRecon || other.data.type != VesselType.Recon)
                    {
                        Vector3 diff = other.transform.position - owner.transform.position;
                        float dist2 = diff.sqrMagnitude;
                        if(dist2 < closestDist2)
                        {
                            closest = other;
                            closestDist2 = dist2;
                        }
                    }
                }
            }

            return closest;
        }

        //
        // --------------------------------------------------------------------
        //

        private Vector3 GetTarget()
        {
            if(attackTarget == null) return owner.transform.position;

            Vector3 diff = attackTarget.transform.position - owner.transform.position;
            Vector3 dir = diff.normalized;
            return attackTarget.transform.position - (dir * targetDist);
        }

        //
        // --------------------------------------------------------------------
        //

        private void AttackUpdate(float dt)
        {
            foreach(WeaponData weapon in owner.data.stats.weapons)
            {
                Fire(weapon);
            }
        }
        
        //
        // --------------------------------------------------------------------
        //

        private WeaponState Fire(WeaponData weapon)
        {
            WeaponState state = null;
            if(wstates.TryGetValue(weapon, out state) == false)
            {
                state = new WeaponState() {
                    lastFired = Time.time
                };
            }

            if(Time.time - state.lastFired > weapon.shotDelaySec)
            {
                state.lastFired = Time.time;
                WeaponData.Fire(owner, attackTarget, weapon);
            }

            wstates[weapon] = state;
            return state;
        }
    }
}
