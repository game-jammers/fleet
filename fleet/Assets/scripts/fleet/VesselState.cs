//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    // what does a vessel do when no commands are active
    public enum AIType
    {
        Aggressive, // attack the nearest thing
        Defensive,  // attack the nearest thing without moving
        Reactive,   // attack only if attacked first
        Passive,    // do nothing
        Coward      // run away
    }

    //
    // ########################################################################
    //

    [System.Serializable]
    public class VesselState
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum Mode
        {
            Reserve,
            TransitEnter,
            TransitExit,
            Deployed
        }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [Header("Info")]
        public string name                                      = System.String.Empty;
        public Mode mode                                        = Mode.Reserve;
        public AIType ai                                        = AIType.Defensive;

        [Header("Transit")]
        public int fuel;
        public float transitAmt;
        public Xform transitTarget;
        public float speed_mps;

        [Header("Defenses")]
        public float shields;
        public float armor;
        public float hull;
        public bool alive                                       { get { return hull > 0f; } }

        [Header("Storage")]
        public ResourceList storage;    

        [Header("Debug")]
        public bool dontAttackRecon                             = false;
        
        //
        // create /////////////////////////////////////////////////////////////
        //

        public static VesselState Create(string name, VesselData data)
        {
            VesselState result = new VesselState() {
                // info
                name = name,
                mode = Mode.Reserve,

                // transit
                fuel = data.stats.maxFuel,
                transitAmt = 0f,
                transitTarget = new Xform() {
                    position = Vector3.zero,
                    rotation = Quaternion.identity,
                    localScale = Vector3.one
                },
                speed_mps = 0f,
        
                // defenses
                shields = data.stats.maxShield,
                armor = data.stats.maxArmor,
                hull = data.stats.maxHull,

                // storage
                storage = new ResourceList(data.stats.storageCapacity)
            };

            return result;
        }
    }
}
