//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{ 
    public class CommandMove
        : BaseCommand
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public virtual Vector3 target                           { get; protected set; }

        protected bool stopping                                   = false;
        protected bool fullStop                                   = true;
        

        //
        // create /////////////////////////////////////////////////////////////
        //

        public CommandMove(Vessel owner, Vector3 target)
            : base(owner)
        {
            this.target = target;
        }

        //
        // public utilities ///////////////////////////////////////////////////
        //

        public static void TurnTowards(Vessel vessel, Vector3 target, float dt)
        {
            VesselStats stats = vessel.data.stats;
            VesselState state = vessel.state;

            Vector3 diff = (vessel.transform.position - target);
            float dist = diff.magnitude;
            Vector3 dir = diff.normalized;
            if(dir.sqrMagnitude > 0f)
            {
                Quaternion targetRot = Quaternion.LookRotation(dir, Vector3.up);
                vessel.transform.rotation = Quaternion.RotateTowards(vessel.transform.rotation, targetRot, stats.turnSpeedDeg * dt);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static float MoveTowards(Vessel vessel, Vector3 target, float dt)
        {
            VesselStats stats = vessel.data.stats;
            VesselState state = vessel.state;

            // step 1: turn towards target
            Vector3 diff = (target - vessel.transform.position);
            float dist = diff.magnitude;
            Vector3 dir = diff.normalized;

            Quaternion targetRot = Quaternion.LookRotation(dir, Vector3.up);

            vessel.transform.rotation = Quaternion.RotateTowards(vessel.transform.rotation, targetRot, stats.turnSpeedDeg * dt);

            // step 2: if we are look at the right direction start accelerating
            float angle = Vector3.SignedAngle(vessel.transform.forward, dir, Vector3.up);
            float timeToTurn = angle / stats.turnSpeedDeg;
            float brakeTime = state.speed_mps / stats.acceleration_mps;
            float timeRemain = dist / state.speed_mps;

            // lean into turns
            {
                Quaternion lean = vessel.view.offsetRot * Quaternion.Euler(0f, 0f, -angle);
                vessel.view.transform.localRotation = Quaternion.RotateTowards(vessel.view.transform.localRotation, lean, 90f * dt);
            }

            if(timeToTurn < timeRemain)
            {
                float deltaspd = stats.acceleration_mps * dt;
                if(brakeTime >= timeRemain)
                {
                    state.speed_mps = btMath.Max(0f, state.speed_mps - deltaspd);
                }
                else
                {
                    state.speed_mps = btMath.Min(stats.maxSpeed_mps, state.speed_mps + deltaspd);
                }
            }

            vessel.transform.position += vessel.transform.forward * state.speed_mps * dt;

            return dist;
        }

        //
        // --------------------------------------------------------------------
        //

        public static float Decelerate(Vessel vessel, float dt)
        {
            VesselState state = vessel.state;
            VesselStats stats = vessel.data.stats;

            state.speed_mps = btMath.Max(0f, state.speed_mps - (stats.acceleration_mps * dt));
            vessel.transform.position += vessel.transform.forward * state.speed_mps * dt;
            return state.speed_mps;
        }
        

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Update(float dt)
        {
            bool finished = false;
            if(!stopping)
            {
                float dist = MoveTowards(owner, target, dt);
                finished = dist <= 1f;
            }
            else if(stopping)
            {
                float speed = Decelerate(owner, dt);
                finished = speed.IsApproximately(0) || !fullStop;
            }

            return finished;
        }

        //
        // --------------------------------------------------------------------
        //

        public override void Interrupt(BaseCommand next)
        {
            stopping = true;
            fullStop = true;

            if(next is CommandMove)
            {
                fullStop = false;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public override void End()
        {
            if(fullStop)
            {
                owner.state.speed_mps = 0f;
            }
        }
    }
}
