//
// (c) GameJammers 2021
// http://www.jamming.games
//

namespace Fleet
{
    public enum VesselCap
    {
        Attack                  = 1,

        MiningOrganic           = 2,
        MiningInorganic         = 4,
        MiningExotic            = 8
    }
}
