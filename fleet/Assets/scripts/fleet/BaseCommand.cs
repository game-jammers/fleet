//
// (c) GameJammers 2021
// http://www.jamming.games
//

namespace Fleet
{
    public abstract class BaseCommand
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vessel owner                                     { get; private set; }

        //
        // init ///////////////////////////////////////////////////////////////
        //
        
        public BaseCommand(Vessel owner)
        {
            this.owner = owner;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual void Start() {}
        public virtual void Interrupt(BaseCommand next) {}
        public abstract bool Update(float dt);
        public virtual void End() {}
    }
}
