//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public enum PlanetType
    {
        Terrestrial,
        Gas,
        Red,
        Lunar,
        Ice,
        Rock
    }

    //
    // ########################################################################
    //

    public class PlanetData
        : IStellarData
    {
        public int seed                                         { get; private set; }
        public StellarType stellarType                          { get; private set; }
        public string name                                      { get; set; }
        public PlanetType type;
        public Sprite image                                     { get { return Database.GetTable<StellarDb>().planetImages[type]; } }
        public Distance size                                    { get; set; }
        public Distance distance                                { get; set; }
        public PlanetData[] moons;
        public ILocalData[] locals                              { get; set; }
        public bool besieged                                    { get; set; }
        public bool alienInstallation                           { get; set; }

        //
        // create /////////////////////////////////////////////////////////////
        //

        private static PlanetData CreateInternal(System.Random rng, string baseName, int planetIdx, int moonIdx, float size, DistanceUnit unit, Distance distance)
        {
            PlanetData result = new PlanetData();
            result.seed = rng.Next();

            //
            // TYPE
            //
            result.type = EnumUtility.Random<PlanetType>(rng);
            result.besieged = false;
            result.alienInstallation = false;

            //
            // NAME
            //
            NamesDb names = Database.GetTable<NamesDb>();
            string suffix = System.String.Empty;
            if(names.suffix.IsValidIndex(planetIdx))
            {
                result.stellarType = StellarType.Planet;
                suffix = names.suffix[planetIdx];
            }
            else if(names.moonNames.IsValidIndex(moonIdx))
            {
                result.stellarType = StellarType.Moon;
                suffix = names.moonNames[moonIdx];
            }
            result.name = System.String.Format("{0} {1}", baseName, suffix);

            //
            // SIZE
            //
            switch(result.type)
            {
                case PlanetType.Terrestrial: result.size = new Distance(rng.Next(size*0.5f,  size*2f),   unit); break;
                case PlanetType.Gas:         result.size = new Distance(rng.Next(size*0.8f,  size*5f),   unit); break;
                case PlanetType.Red:         result.size = new Distance(rng.Next(size*0.5f,  size*2f),   unit); break;
                case PlanetType.Lunar:       result.size = new Distance(rng.Next(size*0.25f, size*1.5f), unit); break;
                case PlanetType.Ice:         result.size = new Distance(rng.Next(size*0.1f,  size*3f),   unit); break;
                case PlanetType.Rock:        result.size = new Distance(rng.Next(size*0.1f,  size*3f),   unit); break;
            }

            //
            // DISTANCE
            //
            result.distance = distance;

            //
            // ASTEROIDS
            //
            int count = rng.Next(0,25);
            result.locals = AsteroidData.Create(rng, (float)result.size.value, (uint)count);

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static PlanetData Create(System.Random rng, string baseName, int planetIdx, Distance dist)
        {
            PlanetData result = CreateInternal(rng, baseName, planetIdx, -1, 13, DistanceUnit.Kilometer, dist);

            int moonCount = rng.Next(0,4);
            result.moons = new PlanetData[moonCount];
            for(int i = 0; i < moonCount; ++i)
            {
                result.moons[i] = CreateInternal(rng, result.name, -1, i, 4, DistanceUnit.Kilometer, dist);
            }
            
            return result;
        }
    }
}
