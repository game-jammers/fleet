//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;

namespace Fleet
{
    public class SolarSystemData
    {
        public int seed;
        public string name;
        public UniverseData.Node universeData;
        public IStellarData[] stellarObjects;
        public bool isSol                                       { get { return universeData.isSol; } }

        //
        // create /////////////////////////////////////////////////////////////
        //

        public static SolarSystemData Create(UniverseData.Node node, System.Random rng)
        {
            SolarSystemData result = new SolarSystemData();
            result.seed = rng.Next();
            result.universeData = node;

            NamesDb names = Database.GetTable<NamesDb>();
            result.name = node.id;

            StarData root = StarData.Create(rng, result.name, 0, new Distance(0, DistanceUnit.AstronomicalUnit));
            int depth = rng.Next(2,10);
            result.stellarObjects = new IStellarData[depth];
            result.stellarObjects[0] = root;

            int starCount = 1;
            int planetCount = 0;
            int beltCount = 0;
            bool sunPossible = true;
            for(int i = 1; i < depth; ++i)
            {
                Distance dist = new Distance(rng.Next(1f, 1.8f)*i, DistanceUnit.AstronomicalUnit);
                float rnd = rng.NextUnit();
                if(sunPossible && rnd > 0.95f)
                {
                    result.stellarObjects[i] = StarData.Create(rng, result.name, ++starCount, dist);
                }
                else
                {
                    sunPossible = false;
                    float next = rng.NextUnit();
                    if(next < 0.8f)
                    {
                        result.stellarObjects[i] = PlanetData.Create(rng, result.name, ++planetCount, dist);
                    }
                    else
                    {
                        result.stellarObjects[i] = BeltData.Create(rng, result.name, ++beltCount, dist);
                    }
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static SolarSystemData CreateSol(UniverseData.Node node)
        {
            SolarSystemData result = new SolarSystemData();
            result.seed = 1337;
            result.universeData = node;
            System.Random rng = new System.Random(result.seed);

            result.name = "Sol";
            result.stellarObjects = new IStellarData[11];

            // sun
            StarData sol = StarData.Create(rng, result.name, 0, new Distance(0, DistanceUnit.AstronomicalUnit));
            sol.type = StarType.Yellow;
            sol.size = new Distance(1391, DistanceUnit.Megameter);
            sol.distance = new Distance(0, DistanceUnit.AstronomicalUnit);
            result.stellarObjects[0] = sol;

            // mercury
            PlanetData mercury = PlanetData.Create(rng, result.name, 1, new Distance(0.39, DistanceUnit.AstronomicalUnit));
            mercury.name = "Mercury (Sol Prime)";
            mercury.type = PlanetType.Rock;
            mercury.size = new Distance(4789, DistanceUnit.Kilometer);
            mercury.locals = AsteroidData.Create(rng, (float)mercury.size.value, 5);
            mercury.moons = null;
            result.stellarObjects[1] = mercury;

            // venus
            PlanetData venus = PlanetData.Create(rng, result.name, 2, new Distance(0.723, DistanceUnit.AstronomicalUnit) - mercury.distance);
            venus.name = "Venus (Sol Secundus)";
            venus.type = PlanetType.Rock;
            venus.size = new Distance(12104, DistanceUnit.Kilometer);
            venus.locals = AsteroidData.Create(rng, (float)venus.size.value, 4);
            venus.moons = null;
            result.stellarObjects[2] = venus;

            // earth
            PlanetData earth = PlanetData.Create(rng, result.name, 3, new Distance(1, DistanceUnit.AstronomicalUnit) - venus.distance);
            earth.name = "Earth (Sol Tetri)";
            earth.type = PlanetType.Terrestrial;
            earth.size = new Distance(12742, DistanceUnit.Kilometer);
            earth.locals = AsteroidData.Create(rng, (float)earth.size.value, 0);
            earth.alienInstallation = true;
            // moons
            {
                PlanetData moon = PlanetData.Create(rng, result.name, 1, new Distance(386, DistanceUnit.Megameter));
                moon.name = "Luna (Sol Tetri Alpha)";
                moon.type = PlanetType.Lunar;
                moon.size = new Distance(4789, DistanceUnit.Kilometer);
                moon.locals = null;
                moon.moons = null;
                earth.moons = new PlanetData[] {
                    moon
                };
            }
            result.stellarObjects[3] = earth;

            // mars
            PlanetData mars = PlanetData.Create(rng, result.name, 3, new Distance(1.524, DistanceUnit.AstronomicalUnit) - earth.distance);
            mars.name = "Mars (Sol Quadrus)";
            mars.type = PlanetType.Red;
            mars.size = new Distance(3389, DistanceUnit.Kilometer);
            mars.locals = AsteroidData.Create(rng, (float)mars.size.value, 3);
            // moons
            {
                PlanetData phobos = PlanetData.Create(rng, result.name, 1, new Distance(6000, DistanceUnit.Kilometer));
                phobos.name = "Phobos (Sol Quadrus Alpha)";
                phobos.type = PlanetType.Lunar;
                phobos.size = new Distance(22, DistanceUnit.Kilometer);
                phobos.locals = null;
                phobos.moons = null;

                PlanetData deimos = PlanetData.Create(rng, result.name, 1, new Distance(20069, DistanceUnit.Kilometer));
                deimos.name = "Deimos (Sol Quadrus Beta)";
                deimos.type = PlanetType.Rock;
                deimos.size = new Distance(13, DistanceUnit.Kilometer);
                deimos.locals = null;
                deimos.moons = null;

                mars.moons = new PlanetData[] {
                    phobos,
                    deimos
                };
            }
            result.stellarObjects[4] = mars;

            //asteroid belt
            BeltData belt = BeltData.Create(rng, result.name, 0, new Distance(3.0, DistanceUnit.AstronomicalUnit) - mars.distance);
            result.stellarObjects[5] = belt;

            // jupiter
            PlanetData jupiter = PlanetData.Create(rng, result.name, 3, new Distance(5.2, DistanceUnit.AstronomicalUnit) - belt.distance);
            jupiter.name = "Jupiter (Sol Pentus)";
            jupiter.type = PlanetType.Gas;
            jupiter.size = new Distance(142.79, DistanceUnit.Megameter);
            jupiter.locals = AsteroidData.Create(rng, (float)jupiter.size.value, 12);
            // moons
            {
                PlanetData europa = PlanetData.Create(rng, result.name, 1, new Distance(6000, DistanceUnit.Kilometer));
                europa.name = "Europa (Sol Pentus Alpha)";
                europa.type = PlanetType.Lunar;
                europa.size = new Distance(22, DistanceUnit.Kilometer);
                europa.locals = null;
                europa.moons = null;

                PlanetData ganymede = PlanetData.Create(rng, result.name, 1, new Distance(20069, DistanceUnit.Kilometer));
                ganymede.name = "Ganymede (Sol Pentus Beta)";
                ganymede.type = PlanetType.Rock;
                ganymede.size = new Distance(13, DistanceUnit.Kilometer);
                ganymede.locals = null;
                ganymede.moons = null;

                PlanetData io = PlanetData.Create(rng, result.name, 1, new Distance(6000, DistanceUnit.Kilometer));
                io.name = "Io (Sol Pentus Gamma)";
                io.type = PlanetType.Lunar;
                io.size = new Distance(22, DistanceUnit.Kilometer);
                io.locals = null;
                io.moons = null;

                PlanetData callisto = PlanetData.Create(rng, result.name, 1, new Distance(20069, DistanceUnit.Kilometer));
                callisto.name = "Callisto (Sol Pentus Delta)";
                callisto.type = PlanetType.Rock;
                callisto.size = new Distance(13, DistanceUnit.Kilometer);
                callisto.locals = null;
                callisto.moons = null;

                jupiter.moons = new PlanetData[] {
                    europa,
                    ganymede,
                    io,
                    callisto
                };
            }
            result.stellarObjects[6] = jupiter;

            // saturn
            PlanetData saturn = PlanetData.Create(rng, result.name, 3, new Distance(9.53, DistanceUnit.AstronomicalUnit) - jupiter.distance);
            saturn.name = "Saturn (Sol Hexus)";
            saturn.type = PlanetType.Gas;
            saturn.size = new Distance(120.66, DistanceUnit.Megameter);
            saturn.locals = AsteroidData.Create(rng, (float)saturn.size.value, 16);
            // moons
            {
                PlanetData titan = PlanetData.Create(rng, result.name, 1, new Distance(1200, DistanceUnit.Megameter));
                titan.name = "Titan (Sol Hexus Alpha)";
                titan.type = PlanetType.Gas;
                titan.size = new Distance(2574, DistanceUnit.Kilometer);
                titan.locals = AsteroidData.Create(rng, (float)titan.size.value, 2);
                titan.moons = null;

                PlanetData enceladus = PlanetData.Create(rng, result.name, 1, new Distance(238, DistanceUnit.Megameter));
                enceladus.name = "Enceladus (Sol Hexus Beta)";
                enceladus.type = PlanetType.Ice;
                enceladus.size = new Distance(504, DistanceUnit.Kilometer);
                enceladus.locals = AsteroidData.Create(rng, (float)enceladus.size.value, 7);
                enceladus.moons = null;

                PlanetData mimas = PlanetData.Create(rng, result.name, 1, new Distance(340, DistanceUnit.Megameter));
                mimas.name = "Mimas (Sol Pentus Gamma)";
                mimas.type = PlanetType.Rock;
                mimas.size = new Distance(396, DistanceUnit.Kilometer);
                mimas.locals = null;
                mimas.moons = null;

                saturn.moons = new PlanetData[] {
                    titan,
                    enceladus,
                    mimas,
                };
            }
            result.stellarObjects[7] = saturn;

            // uranus
            PlanetData uranus = PlanetData.Create(rng, result.name, 3, new Distance(19.18, DistanceUnit.AstronomicalUnit) - saturn.distance);
            uranus.name = "Uranus (Sol Heptis)";
            uranus.type = PlanetType.Gas;
            uranus.size = new Distance(51.12, DistanceUnit.Megameter);
            uranus.locals = AsteroidData.Create(rng, (float)uranus.size.value, 22);
            // moons
            {
                PlanetData ariel = PlanetData.Create(rng, result.name, 1, new Distance(522, DistanceUnit.Megameter));
                ariel.name = "Ariel (Sol Heptis Alpha)";
                ariel.type = PlanetType.Ice;
                ariel.size = new Distance(578, DistanceUnit.Kilometer);
                ariel.locals = AsteroidData.Create(rng, (float)ariel.size.value, 5);
                ariel.moons = null;

                PlanetData oberon = PlanetData.Create(rng, result.name, 1, new Distance(603, DistanceUnit.Megameter));
                oberon.name = "Oberon (Sol Heptis Beta)";
                oberon.type = PlanetType.Ice;
                oberon.size = new Distance(761, DistanceUnit.Kilometer);
                oberon.locals = AsteroidData.Create(rng, (float)oberon.size.value, 7);
                oberon.moons = null;

                PlanetData titania = PlanetData.Create(rng, result.name, 1, new Distance(683, DistanceUnit.Megameter));
                titania.name = "Titania (Sol Heptis Gamma)";
                titania.type = PlanetType.Ice;
                titania.size = new Distance(788, DistanceUnit.Kilometer);
                titania.locals = AsteroidData.Create(rng, (float)titania.size.value, 11);
                titania.moons = null;

                PlanetData umbriel = PlanetData.Create(rng, result.name, 1, new Distance(744, DistanceUnit.Megameter));
                umbriel.name = "Umbriel (Sol Heptis Delta)";
                umbriel.type = PlanetType.Rock;
                umbriel.size = new Distance(584, DistanceUnit.Kilometer);
                umbriel.locals = AsteroidData.Create(rng, (float)umbriel.size.value, 3);
                umbriel.moons = null;

                uranus.moons = new PlanetData[] {
                    ariel,
                    oberon,
                    titania,
                    umbriel,
                };
            }
            result.stellarObjects[8] = uranus;

            // neptune
            PlanetData neptune = PlanetData.Create(rng, result.name, 3, new Distance(30.06, DistanceUnit.AstronomicalUnit) - uranus.distance);
            neptune.name = "Neptune (Sol Octis)";
            neptune.type = PlanetType.Gas;
            neptune.size = new Distance(48.6, DistanceUnit.Megameter);
            neptune.locals = AsteroidData.Create(rng, (float)uranus.size.value, 2);
            // moons
            {
                PlanetData triton = PlanetData.Create(rng, result.name, 1, new Distance(741, DistanceUnit.Megameter));
                triton.name = "Triton (Sol Octis Alpha)";
                triton.type = PlanetType.Ice;
                triton.size = new Distance(2700, DistanceUnit.Kilometer);
                triton.locals = AsteroidData.Create(rng, (float)triton.size.value, 5);
                triton.moons = null;

                neptune.moons = new PlanetData[] {
                    triton
                };
            }
            result.stellarObjects[9] = neptune;

            // pluto
            PlanetData pluto = PlanetData.Create(rng, result.name, 3, new Distance(39.5, DistanceUnit.AstronomicalUnit) - neptune.distance);
            pluto.name = "Pluto (Sol Ix)";
            pluto.type = PlanetType.Gas;
            pluto.size = new Distance(2.2, DistanceUnit.Megameter);
            pluto.locals = AsteroidData.Create(rng, (float)pluto.size.value, 1);
            // moons
            {
                PlanetData charon = PlanetData.Create(rng, result.name, 1, new Distance(441, DistanceUnit.Megameter));
                charon.name = "Charon (Sol Ix Alpha)";
                charon.type = PlanetType.Rock;
                charon.size = new Distance(1200, DistanceUnit.Kilometer);
                charon.locals = null;
                charon.moons = null;

                pluto.moons = new PlanetData[] {
                   charon
                };
            }
            result.stellarObjects[10] = pluto;

            return result;
        }
    }
}
