//
// (c) GameJammers 2021
// http://www.jamming.games
//

using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public interface ILocalData
    {
        Vector3 position                                        { get; }
        float size                                              { get; }
        float jumpWell                                          { get; }
    }
}
