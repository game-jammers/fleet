//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;

namespace Fleet
{
    public enum ResourceType
    {
        Inorganic,
        Organic,
        Exotic
    }

    //
    // ########################################################################
    //

    public struct Resource
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public ResourceType type;
        public int volume;

        public Resource(ResourceType type, int volume)
        {
            this.type = type;
            this.volume = volume;
        }
    }

    //
    // ########################################################################
    //
    
    public class ResourceList
        : EnumList<ResourceType, Resource>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public int maxStorage                                   { get; private set; }
        public int spaceUsed                                    { get { return GetSpaceUsed(); } }
        public int spaceRemaining                               { get { return maxStorage - spaceUsed; } }
        public float spaceRemainingPerc                         { get { return spaceRemaining / (float)maxStorage; } }

        //
        // create /////////////////////////////////////////////////////////////
        //

        public ResourceList()
        {
            maxStorage = int.MaxValue;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public ResourceList(int maxStorage)
        {
            this.maxStorage = maxStorage;
        }
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public bool Add(Resource res)
        {
            if(spaceRemaining >= res.volume)
            {
                res.volume += this[res.type].volume;
                this[res.type] = res;
                return true;
            }

            return false;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Remove(Resource res)
        {
            Resource newres = this[res.type];
            newres.volume = btMath.Max(0, newres.volume - res.volume);
            this[res.type] = newres;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Remove(ResourceList resl)
        {
            foreach(Resource res in resl)
            {
                Remove(res);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public int Max(ResourceList other)
        {
            int max = 0;
            EnumUtility.ForEachValue<ResourceType>((res)=>{
                if(other[res].volume > 0)
                {
                    max = btMath.Max(max, (int)btMath.Floor(this[res].volume / (float)other[res].volume));
                }
            });

            return max;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override string ToString()
        {
            return System.String.Format("Inorganic: {0} | Organic: {1} | Exotic: {2} | Max: {3}/{4}",
                this[ResourceType.Inorganic].volume,
                this[ResourceType.Organic].volume,
                this[ResourceType.Exotic].volume,
                spaceRemaining, maxStorage
            );
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private int GetSpaceUsed()
        {
            int result = 0;
            foreach(Resource res in this)
            {
                result += res.volume;
            }

            return result;
        }
        
    }
}
