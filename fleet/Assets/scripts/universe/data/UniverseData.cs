//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class UniverseData
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public class Node
        {
            public int seed;
            public string id;
            public Sprite image;
            public Vector2Int position;
            public float danger                                 = 0;
            public int dangerLevel                              = 0;
            public bool isSol                                   = false;

            public SolarSystemData CreateSolarSystem()
            {
                if(id == "Sol")
                {
                    return GameManager.instance.player.universe.solData;
                }

                System.Random rng = new System.Random(seed);
                return SolarSystemData.Create(this, rng);
            }          
        }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        
        public int seed;

        public Node[] nodes;
        public Node earth;
        public Node current;

        public SolarSystemData solData;
        public List<IStellarData> alienInstallations            { get { return GetAlienInstallations(); } }
        public int maxAlienInstallations                        { get; private set; }
        public int alienInstallationCount                       { get { return alienInstallations.Count; } }

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public static UniverseData Create(int seed, int size)
        {
            UniverseData result = new UniverseData() {
                seed = seed
            };

            System.Random rng = new System.Random(seed);

            StellarDb sdb = Database.GetTable<StellarDb>();

            result.nodes = new Node[size];
            result.nodes[0] = new Node {
                seed = 1337,
                id = "Sol",
                image = sdb.starImages[StarType.Yellow],
                position = new Vector2Int(0, 0),
                danger = 100f,
                dangerLevel = int.MaxValue,
                isSol = true
            };
            result.solData = SolarSystemData.CreateSol(result.nodes[0]);
            result.maxAlienInstallations = result.alienInstallationCount;

            bool[,] occupancy = new bool[10,10];
            for(int i = 0; i < 10; ++i)
                for(int j = 0; j < 10; ++j)
                    occupancy[i,j] = false;

            int idx = 1;
            while(idx < size)
            {
                Vector2Int pos = new Vector2Int(rng.Next(0,10), rng.Next(0,10));
                if(occupancy[pos.x, pos.y] == false)
                {
                    result.nodes[idx] = new Node {
                        seed = rng.Next(),
                        id = NamesDb.RandomName(rng),
                        image = sdb.starImages[StarType.White],
                        position = (pos * 80) - new Vector2Int(25+rng.Next(-10,10), 25+rng.Next(-10,10))
                    };
                    occupancy[pos.x, pos.y] = true;
                    ++idx;
                }
            }

            return result;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private List<IStellarData> GetAlienInstallations()
        {
            List<IStellarData> result = new List<IStellarData>();
            foreach(IStellarData sd in solData.stellarObjects)
            {
                if(sd.alienInstallation)
                    result.Add(sd);

                if(sd is PlanetData planet)
                {
                    if(planet.moons != null)
                    {
                        foreach(PlanetData moon in planet.moons)
                        {
                            if(moon.alienInstallation)
                                result.Add(moon);
                        }
                    }
                }
            }
            return result;
        }

        
    }
}
