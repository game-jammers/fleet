//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class AsteroidData
        : ILocalData
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public int seed                                         { get; private set; }
        public Vector3 position                                 { get; set; }        
        public float size                                       { get { return btMath.Clamp((inorganic + organic + exotic) * 0.5f, 100f, 500f); } }
        [SerializeField] public float jumpWell                  { get; set; }
        public int inorganic                                    { get; private set; }
        public int organic                                      { get; private set; }
        public int exotic                                       { get; private set; }
        public string description                               { get; private set; }

        //
        // create /////////////////////////////////////////////////////////////
        //
        
        public static AsteroidData Create(System.Random rng, Vector3 position)
        {
            AsteroidData result = new AsteroidData() {
                seed = rng.Next(),
                inorganic = rng.Range(100,1000),
                organic = btMath.Max(0, rng.Range(-100,500)),
                exotic = btMath.Max(0, rng.Range(-1000, 100))
            };

            result.jumpWell = result.size / 4f;
            result.position = position;

            LocalDb localdb = Database.GetTable<LocalDb>();
            result.description = localdb.GetAsteroidDescription(rng, result);

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
    
        public static ILocalData[] Create(System.Random rng, float size, uint count)
        {
            ILocalData[] result = new ILocalData[count];
            float step = size;
            for(uint i = 0; i < count; ++i)
            {
            
                Vector3 pos = new Vector3(rng.NextUnit()-0.5f, 0f, rng.NextUnit()-0.5f) * step * i;
                result[i] = AsteroidData.Create(rng, pos);
            }

            return result;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public int GetResourceRemaining(ResourceType type)
        { 
            switch(type)
            {
                case ResourceType.Inorganic: return inorganic;
                case ResourceType.Organic: return organic;
                case ResourceType.Exotic: return exotic;
            }
            throw new System.NotImplementedException(System.String.Format("Unkonwn resource {0}", type));
        }

        //
        // --------------------------------------------------------------------
        //

        public void Remove(Resource res)
        {
            switch(res.type)
            {
                case ResourceType.Inorganic: inorganic = btMath.Max(0, inorganic - res.volume); break;
                case ResourceType.Organic:   organic = btMath.Max(0, organic - res.volume); break;
                case ResourceType.Exotic:    exotic = btMath.Max(0, exotic - res.volume); break;
                default:
                    throw new System.NotImplementedException(System.String.Format("Unkonwn resource {0}", res.type));
            }
        }
        
    }
}
