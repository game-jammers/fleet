//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public class BeltData
        : IStellarData
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public int seed                                         { get; private set; }
        public StellarType stellarType                          { get { return StellarType.Belt; } }
        public string name                                      { get; set; }
        public Sprite image                                     { get { return Database.GetTable<StellarDb>().asteroidImages.Random(new System.Random(seed)); } }
        public Distance size                                    { get; set; }
        public Distance distance                                { get; set; }
        public ILocalData[] locals                              { get; private set; }
        public bool besieged                                    { get; set; }
        public bool alienInstallation                           { get; set; }
    
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static BeltData Create(System.Random rng, string systemName, int beltIdx, Distance distance)
        {
            string suffix = beltIdx > 0 ? beltIdx.ToString() : System.String.Empty;
            BeltData result = new BeltData() {
                seed = rng.Next(),
                name = System.String.Format("{0} Belt {1}", systemName, suffix),
                size = new Distance(rng.Next(100f, 200f), DistanceUnit.Kilometer),
                distance = distance,
                besieged = false,
                alienInstallation = false
            };

            uint count = (uint)btMath.Round((float)result.size.value / rng.Next(2f, 4f));
            result.locals = AsteroidData.Create(rng, (float)result.size.value, count);

            return result;
        }
        
    }
}
