//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public enum StellarType
    {
        Star,
        Planet,
        Moon,
        Belt
    }

    public interface IStellarData
    {
        int seed                                                { get; }
        StellarType stellarType                                 { get; }
        string name                                             { get; }
        Sprite image                                            { get; }
        Distance size                                           { get; }
        Distance distance                                       { get; }
        ILocalData[] locals                                     { get; }
        bool besieged                                           { get; set; }
        bool alienInstallation                                  { get; set; }
    }
}
