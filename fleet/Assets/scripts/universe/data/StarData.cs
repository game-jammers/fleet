//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace Fleet
{
    public enum StarType
    {
        Blue,
        White,
        Yellow,
        Red
    }

    //
    // ########################################################################
    //
    
    public class StarData
        : IStellarData
    {
        public int seed                                         { get; private set; }
        public StellarType stellarType                          { get { return StellarType.Star; } }
        public StarType type;
        public string name                                      { get; set; }
        public Sprite image                                     { get { return Database.GetTable<StellarDb>().starImages[type]; } }
        public Distance size                                    { get; set; }
        public Distance distance                                { get; set; }
        public ILocalData[] locals                              { get { return null; } }
        public bool besieged                                    { get; set; }
        public bool alienInstallation                           { get; set; }

        public static StarData Create(System.Random rng, string solarSystemName, int starIdx, Distance distance)
        {
            StarData result = new StarData();
            result.seed = rng.Next();
            result.besieged = false;
            result.alienInstallation = false;

            NamesDb names = Database.GetTable<NamesDb>();

            string suffix = System.String.Empty;
            if(starIdx > 0)
            {
                suffix = starIdx.ToString();
                if(names.roman.IsValidIndex(starIdx-1))
                    suffix = names.roman[starIdx-1];
            }

            result.name = System.String.Format("{0} {1}", solarSystemName, suffix);

            result.type = EnumUtility.Random<StarType>(rng);

            const float sunSizeMm = 1391;
            switch(result.type)
            {
                case StarType.Blue: result.size = new Distance(rng.Next(sunSizeMm*0.8f, sunSizeMm*5f), DistanceUnit.Megameter); break;
                case StarType.White: result.size = new Distance(rng.Next(sunSizeMm*0.001f, sunSizeMm*0.1f), DistanceUnit.Megameter); break;
                case StarType.Yellow: result.size = new Distance(rng.Next(sunSizeMm*0.5f, sunSizeMm*2f), DistanceUnit.Megameter); break;
                case StarType.Red: result.size = new Distance(rng.Next(sunSizeMm*10f, sunSizeMm*20f), DistanceUnit.Megameter); break;
            }

            result.distance = distance;
            
            return result;
        }
    }
}
