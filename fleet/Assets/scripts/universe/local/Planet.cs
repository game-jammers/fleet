//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class Planet 
        : BackgroundObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public PlanetType type                                  = PlanetType.Terrestrial;
        public PlanetData data                                  { get { return (PlanetData)stellarData; } }
        
        //
        // create /////////////////////////////////////////////////////////////
        //

        public static Planet Create(PlanetData data)
        {
            System.Random rng = new System.Random(data.seed);

            LocalDb localdb = Database.GetTable<LocalDb>();
            Planet prefab = localdb.GetPlanet(data.type);
            float size = (float)data.size.value;

            Vector3 forward = SceneManager.instance.sceneCam.unityCamera.transform.forward;
            forward.x += rng.Next(-0.25f, 0.25f);
            forward.z += rng.Next(-0.25f, 0.25f);
            Vector3 pos = forward.normalized * 240f; //(rng.Next(0, 255f)+240f);

            Planet result = Instantiate(prefab, pos, Quaternion.identity);
            result.name = data.name;
            result.Initialize(data);
            return result;            
        }

        //
        // --------------------------------------------------------------------
        //

        public void Initialize(PlanetData data)
        {
            base.Initialize((IStellarData)data);
        }
        
        
    }
}
