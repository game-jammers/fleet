//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class Star
        : BackgroundObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public StarType type                                    = StarType.White;
        public Sprite image;
        public StarData data                                    { get { return (StarData)stellarData; } }
        
        //
        // create /////////////////////////////////////////////////////////////
        //

        public static Star Create(StarData data)
        {
            System.Random rng = new System.Random(data.seed);

            LocalDb localdb = Database.GetTable<LocalDb>();
            Star prefab = localdb.GetStar(data.type);
            float size = (float)data.size.value;

            Vector3 forward = SceneManager.instance.sceneCam.unityCamera.transform.forward;
            forward.x += rng.Next(-0.25f, 0.25f);
            forward.z += rng.Next(-0.25f, 0.25f);
            Vector3 pos = forward.normalized * 240f; //(rng.Next(0f, 255f)+240f);

            Star result = Instantiate(prefab, pos, Quaternion.identity);
            result.name = data.name;
            result.Initialize(data);
            return result;            

        }

        //
        // --------------------------------------------------------------------
        //

        public void Initialize(StarData sdata)
        {
            base.Initialize((IStellarData)sdata);
        }
        
        
    }
}
