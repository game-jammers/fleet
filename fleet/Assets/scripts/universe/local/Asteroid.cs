//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class Asteroid
        : LocalObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public AsteroidData data                                { get { return (AsteroidData)localData; } }
        [SerializeField] private Renderer render                = null; 
        [SerializeField] Sprite icon                            = null;

        public bool isBackground                                = false;

        //
        // create /////////////////////////////////////////////////////////////
        //

        public static Asteroid Create(AsteroidData data)
        {
            System.Random rng = new System.Random(data.seed);
            LocalDb localdb = Database.GetTable<LocalDb>();
            Asteroid prefab = localdb.asteroids.Random(rng);
            Asteroid result = Instantiate(prefab, data.position, Quaternion.identity);
            result.Initialize(data);
            return result;            
        }

        //
        // --------------------------------------------------------------------
        //
        
        public virtual void Initialize(AsteroidData data)
        {
            base.Initialize(data);
            Refresh();

            
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh()
        {
            render.material.SetFloat("_Inorganic", (float)data.inorganic);
            render.material.SetFloat("_Organic", (float)data.organic);
            render.material.SetFloat("_Exotic", (float)data.exotic);
            render.transform.localScale = Vector3.one * data.size;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override InspectData GetInspectData()
        {
            InspectData result = new InspectData() {
                icon = icon,
                title = "Asteroid",
                description = data.description
            };

            return result;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void FixedUpdate()
        {
            if(isBackground) return;
            Refresh();
        }
        
    }
}
