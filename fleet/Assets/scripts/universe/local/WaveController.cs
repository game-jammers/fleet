//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    [System.Serializable]
    public class WaveController
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public BaseSpaceSceneManager sm                         { get { return BaseSpaceSceneManager.instance; } }
        public SolarSystemData solarSystem                      { get { return sm.solarSystem; } }
        public UniverseData.Node data                           { get { return solarSystem.universeData; } }

        public WaveData[] waves;
        public float danger                                     { get { return currentWave.danger; } }
        public float waveProgress                               { get { return danger >= 1f ? currentWave.progress : 0f; } }
        public int level                                        { get { return btMath.Min(waves.Length-1, data.dangerLevel); } }
        
        public WaveData currentWave                             { get { return waves[level]; } }
        public WaveData solWave;

        private IStellarData assaultLocation                    = null;
        private float delay                                     = 0f;
        private float assaultChance                             = 0f;
        private float destroyChance                             = 0f;

        private const int kSolWaveSize = 10;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh()
        {
            foreach(WaveData wave in waves)
            {
                wave.Refresh();
            }
        }

        public void Update(float dt)
        {
            currentWave.Update(dt);
            if(currentWave.finished)
            {
                if(data.dangerLevel+1 >= waves.Length)
                {
                    if(solWave.enemyCount < kSolWaveSize && Time.time - solWave.lastPackSpawned > 1f)
                    {
                        solWave.SpawnPack();
                    }
                }
                else
                {
                    data.dangerLevel = btMath.Min(data.dangerLevel+1, waves.Length-1);
                }
            }
            else if(sm.local.data.besieged && currentWave.enemyCount <= 0 && sm.local.vessels.Count > 0)
            {
                sm.local.data.besieged = false;
                delay = 0f;
                assaultLocation = null;
                assaultChance = 0f;
                destroyChance = 0f;
            }

            FleetData fleet = GameManager.instance.player.fleet;
            if(fleet.statics.Count > 0)
            {
                delay += dt;
                if((delay > 1f))
                {
                    delay = 0f;
                    if(assaultLocation == null)
                    {
                        assaultChance += 1f;
                        if(btRandom.Range(0f, assaultChance) > 95f)
                        {
                            FleetData.StaticEntry sentry = fleet.statics.Random();
                            IStellarData proposedTarget = sentry.location;
                            if(proposedTarget != sm.local.data)
                            {
                                assaultLocation = proposedTarget;
                                assaultLocation.besieged = true;
                                assaultChance = 0f;
                                destroyChance = 0f;
                            }
                        }
                    }
                    else
                    {
                        destroyChance += 1f;
                        if(btRandom.Range(0f, destroyChance) > 95f)
                        {
                            int index = btRandom.Range(0, fleet.statics.Count-1);
                            FleetData.StaticEntry entry = fleet.statics[index];
                            fleet.statics.RemoveAt(index);
                            assaultChance = 0f;
                            destroyChance = 0f;
                        }
                    }
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void UpdateSol(float dt)
        {
            data.danger = Mathf.Max(0f, data.danger);
            if(solWave.enemyCount < kSolWaveSize && Time.time - solWave.lastPackSpawned > 1f && data.isSol)
            {
                solWave.SpawnPack();
            }

            if(GameManager.instance.player.universe.alienInstallationCount <= 0)
            {
                sm.local.EndGame(true);
            }
        }
    }
}
