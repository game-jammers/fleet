//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public abstract class LocalObject
        : SpaceObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public ILocalData localData                             { get; private set; }
        public override float jumpWell                          { get { return localData.jumpWell; } }
        public override float visualSize                        { get { return localData.size / 24f; } }

        //
        // init ///////////////////////////////////////////////////////////////
        //
        
        public virtual void Initialize(ILocalData localData)
        {
            this.localData = localData;
        }

        //
        // --------------------------------------------------------------------
        //

        public static LocalObject Create(ILocalData localData)
        {
            if(localData is AsteroidData asteroidData)
            {
                return Asteroid.Create(asteroidData);
            }

            return null;
        }
    }
}
