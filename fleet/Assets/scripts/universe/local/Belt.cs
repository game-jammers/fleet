//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class Belt
        : BackgroundObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public BeltData data                                    { get { return (BeltData)stellarData; } }
        
        //
        // create /////////////////////////////////////////////////////////////
        //

        public static Belt Create(BeltData data)
        {
            System.Random rng = new System.Random(data.seed);

            LocalDb localdb = Database.GetTable<LocalDb>();
            GameObject beltGo = new GameObject("Belt Background");
            Belt belt = beltGo.AddComponent<Belt>();
            for(int i = 0; i < 100; ++i)
            {
                Asteroid prefab = localdb.asteroids.Random(rng);
                Vector3 pos = new Vector3(rng.Next(-500f, 500f), -100f, rng.Next(-500f, 500f));
                Asteroid asteroid = Instantiate(prefab, pos, Quaternion.identity);
                Collider collider = asteroid.GetComponent<Collider>();
                if(collider != null)
                    Destroy(collider);
                asteroid.transform.SetParent(belt.transform);
                asteroid.transform.localScale = Vector3.one * rng.Next(2f, 4f);
                Destroy(asteroid);
            }

            return belt;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Initialize(PlanetData data)
        {
            base.Initialize((IStellarData)data);
        }
        
        
    }
}
