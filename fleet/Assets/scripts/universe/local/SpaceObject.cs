//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public abstract class SpaceObject
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
    
        public InspectData inspectData                          { get { return GetInspectData(); } }
        public abstract float jumpWell                          { get; }
        public abstract float visualSize                        { get; }
        public RangeMarker marker                               { get; private set; }
        public SelectionMarker selection                        { get; private set; }
        public SphereCollider sphereCollider;

        public bool selected                                    { get { return GetSelected(); } set { SetSelected(value); } }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public bool InJumpWell(Vector3 other)
        {
            Vector3 diff = other - transform.position;
            float dist2 = diff.sqrMagnitude;
            return dist2 <= jumpWell * jumpWell;
        }

        //
        // --------------------------------------------------------------------
        //

        public bool InJumpWell(Vector3 other, float well)
        {
            Vector3 diff = other - transform.position;
            float dist2 = diff.sqrMagnitude;
            return (dist2 <= jumpWell * jumpWell) || (dist2 <= well * well);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public bool InJumpWell(RangeMarker marker)
        {
            return InJumpWell(marker.transform.position, marker.range);
        }

        //
        // --------------------------------------------------------------------
        //

        public bool InJumpWell(SpaceObject other)
        {
            return other.InJumpWell(transform.position) || InJumpWell(other.transform.position);
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected abstract InspectData GetInspectData();

        //
        // private methods ////////////////////////////////////////////////////
        //

        private bool GetSelected()
        {
            if(selection == null) return false;
            return selection.selected;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void SetSelected(bool sel)
        {
            if(selection != null)
                selection.selected = sel;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            FxDb fx = Database.GetTable<FxDb>();
            marker = fx.GetFx<RangeMarker>("RangeMarker");
            marker.transform.SetParent(transform);
            marker.transform.localPosition = Vector3.zero;
            marker.range = 0f;

            selection = fx.GetFx<SelectionMarker>("SelectionMarker");
            selection.transform.SetParent(transform);
            selection.transform.localPosition = Vector3.zero;
            selection.selected = false;

            sphereCollider = gameObject.AddComponent<SphereCollider>();
            sphereCollider.radius = visualSize;
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnDestroy()
        {
            BaseSpaceSceneManager sm = BaseSpaceSceneManager.instance;
            if(sm != null)
            {
                sm.local.OnSpaceObjectDestroyed(this);
            }
        }
        
    }
}
