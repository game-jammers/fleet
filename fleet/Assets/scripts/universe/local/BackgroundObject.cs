//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class BackgroundObject
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public IStellarData stellarData                         { get; private set; }

        //
        // create /////////////////////////////////////////////////////////////
        //

        public void Initialize(IStellarData stellarData)
        {
            this.stellarData = stellarData;
            Renderer renderer = GetComponent<Renderer>();
            if(renderer != null)
            {
                renderer.material.SetFloat("_Seed", stellarData.seed % 5000f);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public static BackgroundObject Create(IStellarData data)
        { 
            if(data is StarData starData)
            {
                return Star.Create(starData);
            }
            else if(data is PlanetData planetData)
            {
                return Planet.Create(planetData);
            }
            else if( data is BeltData beltData)
            {
                return Belt.Create(beltData);
            }

            return null;
        }
    }
}
