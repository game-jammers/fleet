//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    [System.Serializable]
    public class WaveData
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct EnemyPack
        {
            public VesselData[] vessels;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public BaseSpaceSceneManager sm                         { get { return BaseSpaceSceneManager.instance; } }
        public SolarSystemData solarSystem                      { get { return sm.solarSystem; } }
        public UniverseData.Node data                           { get { return solarSystem.universeData; } }
        public LocalController local                            { get { return sm.local; } }
        
        public float waveStartDelay                             = 60 * 10f;
        public EnemyPack[] packs;

        public float timeBetweenPacksSec                        = 20f;
        public int packsPerWave                                 = 5;
        public int enemyCount                                   { get { return enemies.Count; } }

        public float danger                                     { get { return btMath.Min(1f, data.danger / waveStartDelay); } }
        public float progress                                   { get { return GetProgressToNextPack(); } }
        public bool finished                                    { get { return data.danger != float.PositiveInfinity && danger >= 1f && enemies.Count <= 0 && totalPacks >= packsPerWave; } }

        public float lastPackSpawned                            { get; set; } = 0f;
        private List<Vessel> enemies                            = new List<Vessel>();
        private int totalPacks                                  = 0;
        private int failedJumps                                 = 0;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh()
        {
            totalPacks = 0;
            failedJumps = 0;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Initialize()
        {
            if(danger >= 1f)
            {
                SpawnPack();
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update(float dt)
        {
            data.danger = btMath.Min(data.danger + dt, waveStartDelay);
            if(local.data.besieged && enemies.Count <= 0 && Time.time - lastPackSpawned > 1f)
            {
                SpawnPack();
                --totalPacks;
            }
            else if(!finished && danger >= 1f && enemies.Count <= 0 && totalPacks < packsPerWave)
            {
                if(lastPackSpawned < 1f)
                    lastPackSpawned = Time.time;

                if(Time.time - lastPackSpawned > timeBetweenPacksSec)
                {
                    if(local.vessels.Count > 0)
                    {
                        SpawnPack();
                    }
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void SpawnPack()
        {
            float range = 250f;
            if(failedJumps > 10)
            {
                range = 100f * failedJumps;
            }
            float r = btRandom.Range(-range, range);
            float theta = btRandom.Range(-180f, 180f);
            Vector3 packCenter = new Vector3(btMath.Sin(theta)*r, 0f, btMath.Cos(theta)*r);

            // save some time and just check the origin for being in a warp well, allow the packs to stick together
            foreach(SpaceObject so in local.spaceObjects)
            {
                if(so.InJumpWell(packCenter))
                {
                    failedJumps += 1;
                    return;
                }
            }
            
            failedJumps = 0;

            EnemyPack pack = packs.Random(local.rng);
            foreach(VesselData enemy in pack.vessels)
            {
                r = btRandom.Range(-50f, 50f);
                theta = btRandom.Range(-180f, 180f);

                Vector3 offset = new Vector3(btMath.Sin(theta)*r, 0f, btMath.Cos(theta)*r);
                Vessel vessel = local.SpawnAtLocation(enemy, packCenter+offset, Quaternion.identity, ControllerType.AI);
                vessel.OnVesselDestroyed += OnEnemyDestroyed;
                enemies.Add(vessel);
            }

            totalPacks++;
            lastPackSpawned = Time.time;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void OnEnemyDestroyed(Vessel v)
        {
            enemies.RemoveAll(v2=>v == v2);
            lastPackSpawned = Time.time;
        }

        //
        // --------------------------------------------------------------------
        //

        private float GetProgressToNextPack()
        {
            if(finished) return 0f;
            if(enemies.Count > 0) return 1f;
            if(danger < 1f) return 0f;
            return (Time.time - lastPackSpawned) / timeBetweenPacksSec;
        }
    }
}
