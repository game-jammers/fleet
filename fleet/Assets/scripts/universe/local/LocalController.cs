//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class LocalController
        : MonoBehaviour
        , ICameraController
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum Action
        {
            CameraMoveX,
            CameraMoveY,
            CameraZoom,
            Select,
            Move
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Camera")]
        public float cameraSpeed                                = 1f;
        public Vector2 margin                                   = new Vector2(0.15f, 0.1f);
        public Vector2 cameraRange                              = new Vector2(100f, 500f);
        public float zoomSpeed                                  = 50f;
        public float cameraZoom                                 = 100f;

        public btCamera cam                                     { get; private set; }
        public bool hasCamera                                   { get { return cam != null; } }

        [Header("Environment")]
        public ProceduralSpace skybox;
        public Light sun;
        [ReadOnly] public GameObject root;

        [Header("Interface")]
        public LocalHUD hud;
        public ContextMenu ctx;
        public InspectWindow inspectWindow;
        public RangeMarker rangePrefab;
        public UIElement failMessage;
        private RangeMarker rangeMarker;
        private InputRouter<Action> input;

        [Header("Local Objects")]
        public Vessel vesselPrefab;
        public IStellarData data                                { get; private set; }
        public List<Vessel> vessels                             { get; private set; }
        public List<Vessel> enemyVessels                        { get { return vessels.FindAll(v=>v.controller == ControllerType.AI); } }
        public List<LocalObject> localObjects                   { get; private set; }
        public IEnumerable<SpaceObject> spaceObjects            { get { return GetSpaceObjects(); } }
        public SpaceObject activeSelection                      { get { return selected.IsValidIndex(0) ? selected[0] : null; } }
        public List<SpaceObject> selected                       { get; private set; }
        [ReadOnly] public BackgroundObject bgObject             = null;

        public UIElement dragbox;
        public bool blockDrag                                   = false;
        private bool isDragging;
        private Vector3 startDrag;
        private Vector3 endDrag;

        [Header("Wave Info")]
        public WaveController waves;

        public System.Random rng                                { get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh(IStellarData data)
        {
            this.data = data;
            rng = new System.Random(data.seed);

            //
            // refresh wave controller
            //
            waves.Refresh();
            
            //
            // refresh skybox
            //
            if(skybox != null)
            {
                skybox.Refresh(data.seed);
                sun.transform.Rotate(new Vector3(0f, rng.Next(-180f, 180f), 0f));
            }

            //
            // rebuild local root
            //
            if(root != null)
                Destroy(root);

            root = new GameObject(System.String.Format("{0} Root", data.name));

            //
            // spawn local stellar object
            //
            bgObject = BackgroundObject.Create(data);
            if(bgObject != null)
            {
                bgObject.transform.SetParent(root.transform);
            }

            // if its a planet, spawn the moons
            if(data is PlanetData pd)
            {
                int step = 1;
                if(pd.moons != null && pd.moons.Length > 0)
                {
                    foreach(PlanetData moonData in pd.moons)
                    {
                        BackgroundObject moon = BackgroundObject.Create(moonData);
                        if(moon != null)
                        {
                            // first moon is close, then they are far away
                            if(step == 1)
                            {
                                moon.transform.localScale = moon.transform.localScale * 0.1f;
                                moon.transform.position *= -0.1f;
                                moon.transform.SetParent(root.transform);
                            }
                            else
                            {
                                moon.transform.position *= step * 100f;
                                moon.transform.SetParent(root.transform);
                            }
                        }

                        step += 1;
                    }
                }
            }

            //
            // spawn locals
            //
            if(data.locals != null)
            {
                foreach(ILocalData localData in data.locals)
                {
                    LocalObject local = LocalObject.Create(localData);
                    if(local != null && !data.alienInstallation || local.transform.position.sqrMagnitude > 25f * 25f)
                    {
                        local.transform.SetParent(root.transform);
                        localObjects.Add(local);
                    }
                }
            }

            //
            // spawn statics
            //
            FleetData fleet = GameManager.instance.player.fleet;
            foreach(FleetData.StaticEntry sobj in fleet.statics)
            {
                if(sobj.location == data)
                {
                    vessels.Add(Vessel.Emplace(vesselPrefab, sobj));
                }
            }

            if(data.alienInstallation && BaseSpaceSceneManager.instance is SpaceSceneManager)
            {
                FleetDb fdb = Database.GetTable<FleetDb>();
                VesselData[] vdata = fdb.FindVessel(Faction.Mythos, v=>v.type == VesselType.Shipyard);
                Dbg.Assert(vdata.Length == 1, "Incorret number of alien shipyards {0}", vdata.Length);
                FleetData.StaticEntry sobj = new FleetData.StaticEntry() {
                    vessel = new FleetData.VesselEntry() {
                        data = vdata[0],
                        state = VesselState.Create("AlienInstallation", vdata[0]),
                    },
                    location = data,
                    xform = new Xform() {
                        position = Vector3.zero,
                        rotation = Quaternion.identity,
                        localScale = Vector3.one
                    },
                    beseiged = false
                };
                Vessel v = Vessel.Emplace(vesselPrefab, sobj);
                v.OnVesselDestroyed += (v)=>{
                    data.alienInstallation = false;
                };
                v.controller = ControllerType.AI;
                vessels.Add(v);
            }

            //
            // does this even do anything?
            //
            if(data is BeltData belt)
            {
                Refresh(belt);
            }
            else if(data is PlanetData planet)
            {
                Refresh(planet);
            }
            else if(data is StarData star)
            {
                Refresh(star);
            }

        }

        //
        // selection //////////////////////////////////////////////////////////
        //

        public void DeselectAll()
        {
            selected.Clear();
            foreach(SpaceObject so in spaceObjects)
            {
                so.selected = false;
            }

            if(ctx.visible)
            {
                ctx.Hide(0.5f, null);
            }

            if(inspectWindow.visible)
            {
                ctx.Hide(0.5f, null);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Select(SpaceObject so)
        {
            if(so != null)
            {
                DeselectAll();
                selected.Add(so);
                so.selected = true;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void MultiSelect(SpaceObject so)
        {
            if(so != null)
            {
                selected.Add(so);
                so.selected = true;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Inspect(SpaceObject so)
        {
            if(so == null)
                inspectWindow.Hide(0.5f, null);
            else
                inspectWindow.Show(1.0f, so.inspectData);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void CenterCameraOn(SpaceObject so)
        {
            if(so != null)
            {
                cam.transform.position = so.transform.position;
            }
        }

        //
        // spawning ///////////////////////////////////////////////////////////
        //

        public void StartJump(IStellarData data)
        {
            hud.menu.Close();
            StartCoroutine(JumpAsync(data));
        }

        //
        // --------------------------------------------------------------------
        //

        public void StartJump(UniverseData.Node data)
        {
            hud.menu.Close();
            StartCoroutine(JumpAsync(data));
        }

        //
        // --------------------------------------------------------------------
        //
        

        public void BeginDragSpawn(FleetData.VesselEntry entry)
        {
            if(rangeMarker == null)
            {
                rangeMarker = Instantiate(rangePrefab, Vector3.zero, Quaternion.identity);
            }

            rangeMarker.transform.position = cam.ScreenToWorld(input.mousePos, new Plane(Vector3.up, Vector3.zero));
            rangeMarker.range = entry.data.stats.jumpWell;
        }

        //
        // --------------------------------------------------------------------
        //

        public void UpdateDragSpawn(FleetData.VesselEntry entry)
        {
            if(rangeMarker != null)
            {
                rangeMarker.valid = true;

                foreach(SpaceObject so in spaceObjects)
                {
                    so.marker.range = so.jumpWell;
                    bool valid = !so.InJumpWell(rangeMarker);
                    so.marker.valid = valid;
                    rangeMarker.valid = rangeMarker.valid && valid;
                }

                rangeMarker.transform.position = cam.ScreenToWorld(input.mousePos, new Plane(Vector3.up, Vector3.zero));
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void EndDragSpawn(FleetData.VesselEntry entry)
        {
            if(rangeMarker.valid)
            {
                SpawnAtCursor(entry);
            }

            foreach(SpaceObject so in spaceObjects)
            {
                Dbg.Assert(so != null, "SpaceObject in spaceObjects is null? how?");
                if(so.marker != null)
                {
                    so.marker.range = 0f;
                }
            }

            Destroy(rangeMarker.gameObject);
            rangeMarker = null;
        }

        //
        // --------------------------------------------------------------------
        //

        public void SpawnAtCursor(FleetData.VesselEntry entry)
        {
            if(entry.data != null)
            {
                Xform target = new Xform() {
                    position = cam.ScreenToWorld(input.mousePos, new Plane(Vector3.up, Vector3.zero)),
                    rotation = Quaternion.identity,
                    localScale = Vector3.one
                };
                Vessel vessel = Vessel.JumpIn(vesselPrefab, entry.data, entry.state, target);
                vessels.Add(vessel);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public Vessel SpawnAtLocation(VesselData data, Vector3 position, Quaternion rotation, ControllerType ctype)
        {
            VesselState vstate = VesselState.Create(data.name, data);
            Vessel v = Vessel.JumpIn(vesselPrefab, data, vstate, new Xform() {
                position = position,
                rotation = rotation,
                localScale = Vector3.one
            });

            if(ctype == ControllerType.AI)
            {
                vstate.ai = AIType.Aggressive;
            }

            v.controller = ctype;
            vessels.Add(v);
            return v;
        }
        

        //
        // --------------------------------------------------------------------
        //

        public void OnSpaceObjectDestroyed(SpaceObject so)
        {
            if(so is Vessel vessel)
            {
                vessels.Remove(vessel);
            }
            else if(so is LocalObject local)
            {
                localObjects.Remove(local);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void EndGame(bool success = false)
        {
            StartCoroutine(EndGameAsync(success));
        }

        //
        // ICameraController //////////////////////////////////////////////////
        //

        public virtual void OnTakeCamera(btCamera cam)
        {
            this.cam = cam;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public virtual void OnReleaseCamera()
        {
            cam = null;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void Refresh(BeltData belt)
        {
            hud.Refresh(belt);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void Refresh(PlanetData planet)
        {
            hud.Refresh(planet);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void Refresh(StarData star)
        {
            hud.Refresh(star);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerable<SpaceObject> GetSpaceObjects()
        {
            foreach(Vessel vessel in vessels) yield return vessel;
            foreach(LocalObject lo in localObjects) yield return lo;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void UpdateCamera()
        {
            if(SceneManager.instance.pause == PauseMode.Cutscene) return;

            Vector2 mpos = new Vector2(
                input.mousePos.x / Screen.width,
                1f - (input.mousePos.y / Screen.height)
            );

            Vector2 deltaCam = Vector2.zero;

            deltaCam.x += input.GetAxis(Action.CameraMoveX);
            deltaCam.y += input.GetAxis(Action.CameraMoveY);
            deltaCam = deltaCam.normalized * cameraSpeed;

            cameraZoom = btMath.Clamp(cameraZoom + input.GetAxis(Action.CameraZoom) * Time.deltaTime * zoomSpeed, cameraRange.x, cameraRange.y);
            
            Vector3 deltaMove = cam.TransformDirection(new Vector3(deltaCam.x, 0f, -deltaCam.y));
            deltaMove.y = 0f;

            cam.transform.position += deltaMove * Time.deltaTime;
            cam.unityCamera.transform.localPosition = cam.unityCamera.transform.forward * -cameraZoom ;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void UpdateSelection()
        {
            if(SceneManager.instance.pause != PauseMode.None) return;

            if(input.GetKeyPressed(Action.Select))
            {
                StartDrag();
            }
            else if(input.GetKey(Action.Select))
            {
                UpdateDrag();
            }

            if(!ctx.visible && input.GetKeyReleased(Action.Select))
            {
                SpaceObject so = cam.MousePick<SpaceObject>();
                if(so != null)
                {
                    Select(so);
                }
                else if(rangeMarker != null)
                {
                    DeselectAll();
                }

                if(isDragging)
                {
                    EndDrag();
                }
            }

            if(input.GetKeyReleased(Action.Move))
            {
                SpaceObject hit = cam.MousePick<SpaceObject>(input.mousePos);
                Vessel hitv = hit as Vessel;
                if(activeSelection is Vessel v)
                {
                    if(hit == null)
                    {
                        ctx.Hide(0.5f, null);
                        foreach(SpaceObject sel in selected)
                        {
                            if(sel is Vessel vsel)
                            {
                                vsel.MoveTo(cam.ScreenToWorld(input.mousePos));
                            }
                        }
                    }
                    else if(hitv != null)
                    {
                        if(hitv.controller == ControllerType.Player)
                        {
                            ctx.Show(v,hitv);
                        }
                        else
                        {
                            foreach(SpaceObject sel in selected)
                            {
                                if(sel is Vessel vsel)
                                {
                                    vsel.Attack(hitv);
                                }
                            }
                        }
                    }
                    else
                    {
                        ctx.Show(v, hit);
                    }
                }
                else if(hit != null)
                {
                    Select(hit);
                    ctx.Show(activeSelection, hit);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void StartDrag()
        {
            startDrag = input.mousePos;
        }
        

        //
        // --------------------------------------------------------------------
        //

        private void UpdateDrag()
        {
            endDrag = input.mousePos;

            Vector3 min = Vector3.Min(startDrag, endDrag);
            Vector3 max = Vector3.Max(startDrag, endDrag);
            Vector3 dims = max - min;

            dragbox.Fade(isDragging, 0f, null);

            Vector3 dragDiff = endDrag - startDrag;
            isDragging = dragDiff.sqrMagnitude > 1f && !blockDrag && rangeMarker == null;

            if(isDragging)
            {
                dragbox.rectTransform.position = min;
                dragbox.rectTransform.sizeDelta = dims;
            } 
        }

        //
        // --------------------------------------------------------------------
        //

        private void EndDrag()
        {
            dragbox.Hide(0f, null);

            if(isDragging)
            {
                isDragging = false;
                endDrag = input.mousePos;

                Vector3[] polygon = cam.ScreenToWorldPolygon(startDrag, endDrag);

                DeselectAll();
                foreach(Vessel v in vessels)
                {
                    if(v.controller == ControllerType.Player && btMath.IsPointInPolygonXZ(polygon, v.transform.position))
                    {
                        MultiSelect(v);
                    }
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator JumpAsyncCommon()
        {
            var wait = new WaitForSeconds(0.1f);
            foreach(Vessel vessel in vessels)
            {
                if(vessel.data.faction == Faction.Human)
                {
                    vessel.JumpOut();
                }
            }

            bool warpFinish = false;
            while(!warpFinish)
            {
                yield return wait;
                

                warpFinish = true;
                foreach(FleetData.VesselEntry v in GameManager.instance.player.fleet.vessels)
                {
                    if(v.state.mode != VesselState.Mode.Reserve)
                    {
                        warpFinish = false;
                        break;
                    }
                }
            }

            foreach(SpaceObject so in spaceObjects)
            {
                Destroy(so.gameObject);
            }
        }

        private IEnumerator JumpAsync(IStellarData data)
        {
            yield return JumpAsyncCommon();
            yield return BaseSpaceSceneManager.instance.fader.ShowAsync(1f);
            Refresh(data);
            yield return BaseSpaceSceneManager.instance.fader.HideAsync(1f);
        }
        
        //
        // --------------------------------------------------------------------
        //

        private IEnumerator JumpAsync(UniverseData.Node data)
        {
            yield return JumpAsyncCommon();
            yield return BaseSpaceSceneManager.instance.fader.ShowAsync(1f);
            SpaceSceneManager sm = BaseSpaceSceneManager.instance as SpaceSceneManager;
            sm.m_solarSystem = data.CreateSolarSystem();
            Refresh(sm.m_solarSystem.stellarObjects[0]);
            yield return BaseSpaceSceneManager.instance.fader.HideAsync(1f);
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator EndGameAsync(bool success = false)
        {
            SceneManager.instance.pause = PauseMode.Cutscene;
            yield return BaseSpaceSceneManager.instance.fader.ShowAsync(1f);
            if(!success)
            {
                yield return failMessage.ShowAsync(2f);
                yield return new WaitForSeconds(3f);
                GameManager.ChangeScene(SceneId.TitleScene);
            }
            else
            {
                GameManager.ChangeScene(SceneId.WinScreen);
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            input = new InputRouter<Action>();
            input.Bind(Action.CameraMoveX, InputAction.FromAxis(KeyCode.A, KeyCode.D));
            input.Bind(Action.CameraMoveY, InputAction.FromAxis(KeyCode.W, KeyCode.S));
            input.Bind(Action.CameraZoom, InputAction.FromAxis(InputAction.Axis.MouseWheel, false, 1f));
            input.Bind(Action.Select, InputAction.FromKey(KeyCode.Mouse0));
            input.Bind(Action.Move, InputAction.FromKey(KeyCode.Mouse1));
            if(margin.x <= 0f) margin.x = 0.01f;
            if(margin.x >= 1f) margin.x = 0.99f;
            if(margin.y <= 0f) margin.y = 0.01f;
            if(margin.y >= 1f) margin.y = 0.99f;

            vessels = new List<Vessel>();
            localObjects = new List<LocalObject>();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Start()
        {
            SceneManager.instance.RequestCamera(this);
            failMessage.Hide(0f, null);
            ctx.Hide(0f, null);
            inspectWindow.Hide(0f, null);
            hud.Initialize();
            selected = new List<SpaceObject>();
            dragbox.Hide(0f, null);
        }

        //
        // --------------------------------------------------------------------
        //
       
        protected virtual void Update()
        {
            if(SceneManager.instance.pause != PauseMode.None)
                return;

            UpdateCamera();
            UpdateSelection();

            #if UNITY_EDITOR
            if(GameManager.instance.verboseDebug)
            {
                Dbg.Log("Local Controller Updating Player");
            }
            #endif

            GameManager.instance.player.Update(Time.deltaTime);

            BaseSpaceSceneManager sm = BaseSpaceSceneManager.instance as SpaceSceneManager;
            if(sm != null)
            {
                if(sm.solarSystem.isSol)
                {
                    waves.UpdateSol(Time.deltaTime);
                }
                else
                {
                    waves.Update(Time.deltaTime);
                }
            }
        }
    }
}
