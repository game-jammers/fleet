//
// (c) GameJammers 2019
// http://jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class IndustryDb
        : DatabaseTable
    {
        //
        // members/////////////////////////////////////////////////////////////////
        //
        
        public Recipe[] recipes                                 { get; private set; }
        public Research[] research                              { get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
            recipes = LoadAll<Recipe>("industry/recipes");
            research = LoadAll<Research>("industry/research");
        }

        //
        // --------------------------------------------------------------------
        //
        
        public Research[] GetDefaultResearch()
        {
            List<Research> result = new List<Research>();
            foreach(Research res in research)
            {
                if(res.requiresResearch.IsNullOrEmpty())
                {
                    result.Add(res);
                }
            }
            return result.ToArray();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public Research[] GetSpecialResearch()
        {
            List<Research> result = new List<Research>();
            foreach(Research res in research)
            {
                if(res.requiresResearch.Length == 1 && res.requiresResearch[0] == res)
                    result.Add(res);
            }
            return result.ToArray();
        }
    }
}
