//
// (c) GameJammers 2019
// http://jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class LocalDb
        : DatabaseTable
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Asteroid[] asteroids                             { get; private set; }
        public Planet[] planets                                 { get; private set; }
        public Star[] stars                                     { get; private set; }

        public string[] generalAsteroidDesc                     { get; private set; }
        public string[][] asteroidDesc                          { get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
            asteroids = LoadAll<Asteroid>("universe/asteroids");
            planets = LoadAll<Planet>("universe/planets");
            stars = LoadAll<Star>("universe/stars");

            asteroidDesc = new string[EnumUtility.Count<ResourceType>()][];

            TextAsset[] allAsteroidDesc = LoadAll<TextAsset>("universe/asteroids/descriptions");
            foreach(TextAsset desc in allAsteroidDesc)
            {
                ResourceType type = ResourceType.Inorganic;
                if(EnumUtility.Parse<ResourceType>(desc.name, out type, true, true))
                {
                    asteroidDesc[(int)type] = desc.text.Split('\n');
                }
                else
                {
                    generalAsteroidDesc = desc.text.Split('\n');
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public Planet GetPlanet(PlanetType type)
        {
            return System.Array.Find(planets, p=>p.type == type);
        }

        //
        // --------------------------------------------------------------------
        //

        public Star GetStar(StarType type)
        {
            return System.Array.Find(stars, s=>s.type == type);
        }

        //
        // --------------------------------------------------------------------
        //

        public string GetAsteroidDescription(System.Random rng, AsteroidData data)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(generalAsteroidDesc.Random(rng));

            ResourceType dominantType = ResourceType.Inorganic;
            if( data.organic >= data.inorganic && data.organic >= data.exotic)
                dominantType = ResourceType.Organic;
            else if(data.exotic >= data.inorganic && data.exotic >= data.organic)
                dominantType = ResourceType.Exotic;

            int count = rng.Next(1,3);
            int[] used = new int[count];
            used.Fill(-1);
            for(int i = 0; i < count; ++i )
            {
                int idx = asteroidDesc[(int)dominantType].RandomIndex();
                if(used.IndexOf(idx) < 0)
                {
                    sb.Append(asteroidDesc[(int)dominantType].Random());
                    used[i] = idx;
                }
            }

            return sb.ToString();
        }
        
    }
}
