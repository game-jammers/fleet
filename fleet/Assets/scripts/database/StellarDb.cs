//
// (c) GameJammers 2019
// http://jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class StellarDb
        : DatabaseTable
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Dictionary<PlanetType, Sprite> planetImages;
        public Dictionary<StarType, Sprite> starImages;
        public Sprite[] asteroidImages;

        //
        // ////////////////////////////////////////////////////////////////////////
        //
        
        public override void Initialize(Database db)
        {
            Sprite[] planetSprites = LoadAll<Sprite>("sprites/planets");
            Sprite[] starSprites = LoadAll<Sprite>("sprites/stars");
            asteroidImages = LoadAll<Sprite>("sprites/asteroids");

            planetImages = new Dictionary<PlanetType, Sprite>();
            foreach(Sprite sprite in planetSprites)
            {
                PlanetType type = EnumUtility.Parse<PlanetType>(sprite.name, true);
                planetImages[type] = sprite;
            }

            starImages = new Dictionary<StarType, Sprite>();
            foreach(Sprite sprite in starSprites)
            {
                StarType type = EnumUtility.Parse<StarType>(sprite.name, true);
                starImages[type] = sprite;
            }
        }
    }
}

