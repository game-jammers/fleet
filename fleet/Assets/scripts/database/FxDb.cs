//
// (c) GameJammers 2019
// http://jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class FxDb
        : DatabaseTable
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public GameObject[] allFx;
        public ParticleSystem[] particleFx;
        
        //
        // public methods /////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
            allFx = LoadAll<GameObject>("fx");
            particleFx = LoadAll<ParticleSystem>("fx");
        }

        //
        // --------------------------------------------------------------------
        //

        public T GetFx<T>(string name)
            where T: UnityEngine.Component
        {
            GameObject prefab = GetFx(name);
            GameObject go = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
            T result = go.GetComponent<T>();
            if(result == null)
                GameObject.Destroy(go);
            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public GameObject GetFx(string name)
        {
            return System.Array.Find(allFx, f=>f.name == name);
        }

        //
        // --------------------------------------------------------------------
        //

        public ParticleSystem GetParticle(string name)
        {
            return System.Array.Find(particleFx, p=>p.name == name);
        }
    }
}
