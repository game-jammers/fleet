//
// (c) GameJammers 2019
// http://jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Fleet
{
    public class FleetDb
        : DatabaseTable
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public VesselData[] allVessels;
        public VesselData[] humanVessels;
        public VesselData[] mythosVessels;
        
        //
        // public methods /////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
            List<VesselData> humanv = new List<VesselData>();
            List<VesselData> mythosv = new List<VesselData>();
            allVessels = LoadAll<VesselData>("vessels");

            foreach(VesselData vessel in allVessels)
            {
                switch(vessel.faction)
                {
                    case Faction.Human: humanv.Add(vessel); break;
                    case Faction.Mythos: mythosv.Add(vessel); break;
                }
            }

            humanVessels = humanv.ToArray();
            mythosVessels = mythosv.ToArray();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public VesselData[] FindVessel(Faction faction, System.Predicate<VesselData> predicate)
        {
            VesselData[] data = allVessels;
            switch(faction)
            {
                case Faction.Human: data = humanVessels; break;
                case Faction.Mythos: data = mythosVessels; break;
            }
            
            return System.Array.FindAll(data, predicate);
        }

        //
        // ------------------------------------------------------------------------
        //
        public VesselData[] FindVessel(System.Predicate<VesselData> predicate)
        {
            return System.Array.FindAll(allVessels, predicate);
        }
        
    }
}
