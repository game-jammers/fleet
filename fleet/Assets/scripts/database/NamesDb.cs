//
// (c) GameJammers 2019
// http://jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Fleet
{
    public class NamesDb
        : DatabaseTable
    {
        //
        // members/////////////////////////////////////////////////////////////////
        //
        
        public string[] suffix                                  { get; private set; }
        public string[] moonNames                               { get; private set; }
        public string[] roman                                   { get; private set; }

        private static readonly string kNameChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        
        //
        // public methods /////////////////////////////////////////////////////////
        //

        public override void Initialize(Database db)
        {
            TextAsset[] texts = LoadAll<TextAsset>("universe/names");
            foreach(TextAsset text in texts)
            {
                if(text.name == "suffix")
                    suffix = text.text.Split('\n');
                else if(text.name == "moonnames")
                    moonNames = text.text.Split('\n');
                else if(text.name == "roman")
                    roman = text.text.Split('\n');
                else
                    Dbg.Warn("Unknown names text file {0}", text.name);
            }
        }

        public static string RandomName(System.Random rng)
        {
            byte[] bytes = new byte[5];
            rng.NextBytes(bytes);
            return System.String.Format("{0}{1}{2}{3}{4}-{5}{6}",
                kNameChars.Random(rng),
                kNameChars.Random(rng),
                kNameChars.Random(rng),
                kNameChars.Random(rng),
                kNameChars.Random(rng),
                rng.Next(0,9),
                rng.Next(0,9)
            );
        }
    }
}
