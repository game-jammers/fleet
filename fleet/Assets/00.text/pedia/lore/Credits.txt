
Solo Space Jam by Howard N Smith
https://letsmake.games | https://gitlab.com/letsmakegames
https://jamming.games | https://gitlab.com/game-jammers

Ressurect 64 Palette
Kerrie Lake https://lospec.com/palette-list/resurrect-64

Cartoon FX Particle Effects
Jean Moreno https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-free-109565

Music
 - Beautiful Oblivion
 - Luminance
 - Where Stars Fall
By Scott Buckley
https://www.scottbuckley.com.au/library/beautiful-oblivion/
https://www.scottbuckley.com.au/library/luminance/

"Dialog" SFX
The Atomic Brain
https://freesound.org/people/TheAtomicBrain/sounds/394082/

Fonts by Google

Stone, Leaf, Diamond icon
made by Freepik www.flaticon.com

Some prototying made using Markom3d's Kitbash Set
https://www.blendernation.com/2020/06/30/free-kitbash-set/

as well as a spacehead's greebles kit
spacehead
https://blendswap.com/blend/14778

(I don't think these made it into the final game though)

Textures used at some point (although not all of them made it into the final game)
Aleksandar Pasaric
https://www.pexels.com/photo/urban-photo-of-an-alley-2411688
https://www.pexels.com/photo/photo-of-cars-parked-on-street-3029376

cottonbro
https://www.pexels.com/photo/blue-yellow-and-red-coated-wires-4480541/

skitterphoto
https://www.pexels.com/photo/gray-gold-and-red-industrial-machine-675987/

Yoss Tradore
https://www.pexels.com/photo/monochrome-photo-of-high-rise-building-2526491/

Matheus Bertelli
https://www.pexels.com/photo/green-leafed-trees-1144687/

Pixaby
https://www.pexels.com/photo/brown-mushroom-53494/

Ashish
https://www.pexels.com/photo/closeup-photo-of-white-mushrooms-382040/

Vlad Kovriga
https://www.pexels.com/photo/brown-mushrooms-344061/

Greeble Textures
katsukagi
https://3dtextures.me/2019/08/28/techno-greeble-001/

Aerial Rocks, brown mud rock, coral mud textures
texturehaven.com
