MINING is the only way to acquire important materials used in MANUFACTURING.  In order to mine, you must first select a deployed ship capable of mining by left clicking it in space, or from the fleet window.

With a mining capable vessel selected, right click on an asteroid to bring up the CONTEXT MENU.  This will give you options for mining the different resource types, if the asteroid contains them.  INORGANIC material is by far the most common material, and is used in almost ALL manufacturing jobs.

ORGANIC materials are slightly less common, and are especially important in the production of ships, AI cores, and weaponry.

EXOTIC materials are extremely rare, and are needed for exceptionally advanced production.
