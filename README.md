# fleet

Real time fleet battling sim weekend gamejam

Started: 2021-06-04 15:02:30

## Requirements
 - Unity3D 2020.3.10f1
 - Built on the Universal Render Pipeline

## Credits
 - Repository icon made by [Nhor Phai]("https://www.flaticon.com/authors/nhor-phai") from [Flaticon](www.flaticon.com)

 - Ressurect 64 Palette by Kerrie Lake https://lospec.com/palette-list/resurrect-64
